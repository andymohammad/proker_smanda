<?php

Route::get('/', function () {
    $proker = App\Proker::get();
    return view('landing', compact('proker'));
});
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::group(['middleware' => 'auth'], function () {
    Route::get('/konfigurasi','AdminController@konfigurasi')->middleware('admin')->name('konfigurasi');
    Route::post('/konfigurasi','AdminController@postkonfigurasi')->middleware('admin')->name('postkonfigurasi');
    Route::put('/konfigurasi/{npsn}','AdminController@updatekonfigurasi')->middleware('admin')->name('updatekonfigurasi');
    Route::get('/beranda','AuthController@beranda');
    Route::get('/proker','ProkerController@index')->middleware('admin');
    Route::get('/proker2','ProkerController@index2')->name('prokerindex');
    Route::post('/proker/revadd','ProkerController@revstore');
    Route::post('/proker/add','ProkerController@store');
    Route::get('/proker/{id}','ProkerController@show');
    Route::get('/proker/detail/{id}','ProkerController@detailproker')->name('detailproker');
    Route::post('/proker/detail/{id}/submit','ProkerController@submitproker')->name('submit_proker');
    Route::delete('/proker/{id}','ProkerController@delete');
    Route::get('/proker/file/{id}','ProkerController@viewfileproker')->name('viewupprok');
    Route::post('/proker/file','ProkerController@uploadfileproker')->name('upprok');
    Route::put('/proker/{id}', 'ProkerController@update');
    Route::get('/proker/view-pdf/{id}','ProkerController@viewpdf')->name('viewpdf');
    
    Route::get('/pengguna','PenggunaController@index')->middleware('admin');
    Route::get('/pengguna/changestatus','PenggunaController@changestatus')->middleware('admin');
    Route::post('/pengguna/post','PenggunaController@store')->name('tambahpengguna')->middleware('admin');
    
    Route::get('/profil-saya','MyprofilController@index');

    Route::get('/rekapitulasi','ProkerController@rekapitulasi')->name('rekaproker');

    Route::get('/approve','KepsekController@index')->name('approve')->middleware('kepsek');
    Route::get('/approve/change','KepsekController@changeapprove')->middleware('kepsek');
    Route::get('/approve/appall','KepsekController@approveall')->name('appall')->middleware('kepsek');
    // Route::get('/approve', function(){
    //     return view('pages.approve');
    // })->middleware('kepsek');
    
    Route::get('/file-proker','FileController@index');

    Route::get('/logout','AuthController@logout')->name('logout');
});