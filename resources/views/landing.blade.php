<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>JOKERS | Program Kerja Sekolah</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('alt/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('alt/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('alt/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('alt/css/AdminLTE.min.css')}}">
    <link rel="icon" href="{{ asset('logo/logosda.png')}}">
    <link rel="stylesheet" href="{{ asset('alt/css/skins/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{ asset('alt/DataTables-1.10.18/css/dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('alt/Responsive-2.2.2/css/responsive.bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-green layout-top-nav">
    
    <div class="wrapper">
        
        <header class="main-header">
            <nav class="navbar navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="#" class="navbar-brand"><b>Joker's</b></a>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                    
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    {{-- <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                            <li><a href="#">Link</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-left" role="search">
                            <div class="form-group">
                                <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                            </div>
                        </form>
                    </div> --}}
                    <!-- /.navbar-collapse -->
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <li>
                                <a href="/login"><i class="fa fa-user"></i>&nbsp;
                                    <strong>LOGIN</strong>
                                </a>
                            </li>
                            <!-- /.messages-menu -->
                            
                            <!-- Notifications Menu -->
                            {{-- <li class="dropdown notifications-menu">
                                <!-- Menu toggle button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning">10</span>
                                </a>
                            </li> --}}
                            <!-- Tasks Menu -->
                            
                        </ul>
                    </div>
                    <!-- /.navbar-custom-menu -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </header>
        <!-- Full Width Column -->
        <div class="content-wrapper">
            <div class="container">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Mana<strong>j</strong>emen Pr<strong>o</strong>gram <strong>Ker</strong>ja <strong>S</strong>ekolah 
                        <small>Program Kerja Wakil Kepala Sekolah</small>
                    </h1>
                </section>
                
                <!-- Main content -->
                <section class="content">
                    
                    <div class="callout callout-info">
                        <h4>Perhatian!</h4>
                        <p>
                            Aplikasi ini masih dalam tahap pengembangan, maka akan ada banyak perubahan pada sistem !
                        </p>
                    </div>
                    <div class="box box-default">
                        <div class="box-header with-border">
                            <h3 class="box-title">Daftar Program Kerja Wakil Kepala SMAN 2</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-striped table-bordered dt-responsive nowrap" id="TabelProker">
                                <thead>
                                    <tr>
                                        <th width="2px" >#</th>
                                        <th class="text-center">KEGIATAN</th>
                                        <th class="text-center">SASARAN</th>
                                        <th class="text-center">STATUS</th>
                                        {{-- <th>PENANGGUNG JAWAB</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($proker as $p)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        <td>{{$p->kegiatan}}</td>
                                        <td>{{$p->sasaran}}</td>
                                        @if ($p->status === 'Terlaksana')
                                        <td>
                                            <span class="label label-success">{{$p->status}}</span>
                                        </td>
                                        
                                        @else
                                        <td>
                                            <span class="label label-danger">{{$p->status}}</span> 
                                        </td>
                                        @endif
                                        
                                        {{-- <td>{{$p->user->name}}</td> --}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <div class="container">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 0.1
                </div>
                <strong>Copyright &copy; 2019 <a href="https://www.instagram.com/muhammadd.andy/">Muhammad Andy</a>.</strong> All rights
                reserved.
            </div>
            <!-- /.container -->
        </footer>
    </div>
    <!-- ./wrapper -->
    
    <!-- jQuery 3 -->
    <script src="{{ asset('alt/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('alt/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('alt/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ asset('alt/fastclick/lib/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('alt/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('alt/js/demo.js')}}"></script>
    
    <script src="{{ asset('alt/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('alt/DataTables-1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('alt/Responsive-2.2.2/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('alt/Responsive-2.2.2/js/responsive.bootstrap.min.js')}}"></script>
    <script>
        $(document).ready( function () {
            
            $('#TabelProker').DataTable();
            
        } );
    </script>
</body>
</html>
