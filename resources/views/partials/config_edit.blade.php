<div class="modal fade" id="ModalEditConfig" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="FormEditConfig" role="form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Edit Konfigurasi Aplikasi</h4>
                </div>
                <div class="modal-body">
                    {{-- <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-config-errors">
                        </ul>
                    </div> --}}
                    <div class="form-group">
                        <label>NPSN SEKOLAH</label>
                        <input type="text" name="npsn_up" class="form-control" value="{{$npsn}}" autocomplete="off">
                    </div>
                    <!-- /.form group -->
                    <div class="form-group">
                        <label>NAMA SEKOLAH</label>
                        <input type="text" name="nama_sekolah_up" class="form-control" value="{{$namsek}}" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Tahun Pelajaran</label>
                        <select id="tapel" class="form-control" name="tapel_up" required>
                            @foreach ($config as $a)
                            <option value="{{$a->tapel}}" selected disabled>{{$a->tapel}}</option>
                            @endforeach
                            @for ($i = $now; $i < $now+10; $i++)
                            @php
                            $j = $i + 1
                            @endphp
                            <option value="{{$i . "/" . $j}}">
                                {{$i . "/" . $j}}
                            </option>
                            @endfor
                        </select>
                    </div>                    
                </div>
                <div class="modal-footer">
                    {{-- <input type="hidden" name="proker_id" class="form-control"> --}}
                    <button type="button" class="btn btn-danger pull-left closemodal" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-success" id="btnsaveconf">Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>