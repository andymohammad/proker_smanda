<div class="modal fade" id="AddUser">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{route('tambahpengguna')}}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        Tambah Pengguna
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;">Email</label>
                        <div class="col-sm-9">
                            <input type="email" name="email" class="form-control" placeholder="Masukkan Email ...." autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;">Nama Lengkap</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap User">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;">Level</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="role" id="">
                                <option value="admin">Admin</option>
                                <option value="kepsek">Kepala Sekolah</option>
                                <option value="kurikulum">Wakabid Kurikulum</option>
                                <option value="humas">Wakabid Humas</option>
                                <option value="sarpras">Wakabid Sarana & Prasarana</option>
                                <option value="kesiswaan">Wakabid Kesiswaan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="pwd" class="form-control" id="pwd1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" style="text-align: left;">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="pwd" class="form-control" id="pwd2">
                            <span class="help-block" id="msg"></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn bg-blue-active" id="btnsave" disabled>Simpan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>