<div class="modal fade" id="ModalApprove">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="FormDeleteProker">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="delete-title">Approve Program Kerja</h4>
                </div>
                <div class="modal-body">
                    <p style="text-align:center;">Dengan menekan tombol <button type="button" class="btn btn-xs btn-primary" id="btn-delete">Approve</button>&nbsp;, maka seluruh 
                    program kerja yang berstatus <button class="btn btn-xs btn-warning"><i class="fa fa-hourglass-start"></i> Waiting</button> &nbsp; akan
                    berubah menjadi <button class="btn btn-xs btn-success"><i class="fa fa-check-circle"></i> Approve</button>.
                    </p>
                </div>
                <div class="modal-footer">
                    <input id="proker_id" name="proker_id" type="hidden" value="0">
                    <button type="button" class="btn btn-default closemodal pull-left" data-dismiss="modal">Batal</button>
                    <a href="{{ route('appall')}}" class="btn btn-primary" id="btn-delete">Approve</a>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>