<div class="modal fade" id="ModalDeleteProker">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form id="FormDeleteProker">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="delete-title"></h4>
                </div>
                <div class="modal-body">
                    
                    {{-- <div class="form-group">
                        <label>Penanggung Jawab</label>
                    </div> --}}
                    <div class="form-group">
                        <label>Program / Kegiatan</label>
                        <input type="text" name="kegiatan" class="form-control" disabled>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="proker_id" name="proker_id" type="hidden" value="0">
                    <button type="button" class="btn btn-default closemodal pull-left" data-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" id="btn-delete">Hapus Data</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>