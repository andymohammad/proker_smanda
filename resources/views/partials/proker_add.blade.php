@php
$now = \Carbon\Carbon::now()->format('Y');
$sd = $now + 1;
$tapel_pil = $now . "/" . $sd;
@endphp
<div class="modal fade" id="addProker" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="FormAddProker" method="POST" enctype="multipart/form-data" action="/proker/revadd">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Tambah Program Kerja - Tahun Pelajaran {{$tapel}}</h4>
                </div>
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label>Penanggung Jawab</label>
                        @foreach ($role as $a)
                        <input type="hidden" name="user_id" class="form-control" value="{{$a->id}}">
                        <input type="text" name="role" class="form-control col-8" value="{{"Wakil Kepala Sekolah Bidang " . ucwords($a->role)}}" disabled>
                        <input type="hidden" name="tapel" class="form-control col-8" value="{{$tapel}}">
                        @endforeach
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pelaksanaan</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="tgl_pelaksanaan" autocomplete="off" class="form-control pull-right datepicker" id="datepicker">
                        </div>
                        <!-- /.input group -->
                    </div>
                    <!-- /.form group -->
                    <div class="form-group">
                        <label>Program / Kegiatan</label>
                        <input type="text" name="kegiatan" class="form-control" required autocomplete="off">
                    </div>
                    {{-- <div class="form-group">
                        <label>Tahun Pelajaran</label>
                        <select id="tapel" class="form-control" name="tapel">
                            
                            <option value="{{$tapel}}" selected>{{$tapel}}</option>
                            
                            @for ($i = $now; $i < $now+10; $i++)
                            @php
                            $j = $i + 1
                            @endphp
                            <option value="{{$i . "/" . $j}}">
                                {{$i . " / " . $j}}
                            </option>
                            @endfor
                        </select>
                    </div> --}}
                    <div class="form-group">
                        <label>Sasaran</label>
                        <input type="text" name="sasaran" class="form-control" required autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>File</label>
                        <input type="file" accept="application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" name="filekegiatan" class="form-control">
                        <span class="help-block" style="color:darkorange;"><i class="fa fa-warning"></i> Silahkan upload file pdf / word (.doc, .docx) !</span>
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn-add">Simpan Kegiatan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>