<div class="modal fade" id="ModalEditProker" style="display: none;" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="FormEditProker" enctype="multipart/form-data" role="form" autocomplete="new-password">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Edit Program Kerja</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="edit-error-bag">
                        <ul id="edit-proker-errors">
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Penanggung Jawab</label>
                            @foreach ($role as $a)
                            <input type="hidden" name="user_id" class="form-control" value="{{$a->id}}" disabled>
                            <input type="text" name="role" class="form-control" value="{{ucwords($a->role)}}" disabled>
                            @endforeach
                        </div>
                        <div class="col-sm-8">
                            <label>Nomor SK</label>
                            <input type="text" name="sk" class="form-control">
                        </div>
                    </div>
                    <br>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Tanggal Pelaksanaan</label>
                            
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" autocomplete="off" name="tgl_pelaksanaan" class="form-control pull-right datepicker" id="datepicker">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <label>Tujuan Program</label>
                            <input type="text" name="tujuan" class="form-control">
                        </div>
                    </div>
                    <br>
                    <!-- /.input group -->
                    {{-- </div> --}}
                    <!-- /.form group -->
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <label>Program / Kegiatan</label>
                            <input type="text" name="kegiatan" class="form-control" autocomplete="off">
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <label>Sasaran</label>
                            <input type="text" name="sasaran" class="form-control" autocomplete="off">
                            
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <label>Indikator Ketercapaian Program</label>
                            <input type="text" name="indi" class="form-control">
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <label>File</label>
                        <input type="file" name="filekeg" id="pdf" class="form-control" autocomplete="off" accept="application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" >
                        <span class="help-block" style="color:darkorange;"><i class="fa fa-warning"></i> Silahkan upload file pdf / word (.doc, .docx) !</span>
                    </div> --}}
                    
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="proker_id" class="form-control">
                    <button type="button" class="btn btn-default pull-left closemodal" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn-edit">Simpan Kegiatan</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="ModalFileProker">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <form id="FormFileProker" enctype="multipart/form-data" method="POST" action="{{route('upprok')}}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="file-title"></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="proker_id" name="proker_id">
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <input type="file" id="exampleInputFile" name="uploadfileproker">
                        <p class="help-block">Ekstensi file : .pdf | .doc | .docx.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="proker_id" name="proker_id" type="hidden" value="0">
                    <button type="button" class="btn btn-default closemodal pull-left" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" id="btn-upload">Upload</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>