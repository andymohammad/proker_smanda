@extends('layout.app')

@section('headkonten')
Detail Program Kerja
@endsection
@section('konten')
<style>
    th{
        text-align:center;
    }
    td{
        text-align: center;
    }
</style>
<div class="box box-success">
    @foreach ($proker as $a)
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-tasks"></i> Program Kerja {{$a->user->role}} 
                    <small class="pull-right">
                        Tanggal Pelaksanaan : 
                        @if (!empty($r->tgl_pelaksanaan)) 
                        {{$a->tgl_pelaksanaan}}
                        @else 
                        <span class="label label-danger">Belum ditentukan</span>
                        @endif
                    </small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                Pelaksana
                <address>
                    <strong>{{$a->user->name}}</strong><br>
                </address>                
            </div>
            <div class="col-sm-4 invoice-col">
                Kegiatan
                <address>
                    <strong>{{$a->kegiatan}}</strong><br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                Sasaran Kegiatan
                <address>
                    <strong>{{$a->sasaran}}</strong><br>
                </address>
            </div>
            @endforeach
            
            @foreach ($detproker as $det)
            <div class="col-sm-4 invoice-col">
                NIP
                <address>
                    <strong>{{$det->nip}}</strong><br>
                </address>
            </div>
            <div class="col-sm-4 invoice-col">
                Tujuan Kegiatan
                <address>
                    <strong>{{$det->tujuan}}</strong><br>
                </address>
            </div>
            <div class="col-sm-4 invoice-col">
                Sumber Daya Kegiatan
                <address>
                    <strong>{{$det->sumber_daya}}</strong><br>
                </address>
            </div>
            @endforeach
            <!-- /.col -->
        </div>
        <!-- /.row -->
        
        <!-- Table row -->
        
        <div class="row">
            <table class="table table-striped table-bordered table-responsive">
                <thead class="bg-grey"> 
                    <tr style="background-color:darkolivegreen; color:white;">
                        <th>NO.</th>
                        <th>TUJUAN</th>
                        <th>SASARAN</th>
                        <th>KEGIATAN</th>
                        <th>INDIKATOR KEBERHASILAN</th>
                        <th style="width:10px;">PENANGGUNG JAWAB / PELAKSANA</th>
                        <th>BATAS WAKTU</th>
                        <th style="width:10px;">PROSES M & E</th>
                        <th>SUMBER DAYA</th>
                        <th>KETERANGAN</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($proker as $p)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$p->tujuan}}</td>
                        <td>{{$p->sasaran}}</td>
                        <td>{{$p->kegiatan}}</td>
                        <td>{{$p->indikator}}</td>
                        <td>{{ucwords($p->user->role)}}</td>
                        <td>{{$p->batas_waktu}}</td>
                        <td>{{$p->proses_me}}</td>
                        <td>{{$p->sumber_daya}}</td>
                        <td>{{$p->keterangan}}</td>
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.row -->       
        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                {{-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> --}}
                
                @php
                $id_dec = Crypt::decrypt($id);
                $query = DB::table('proker')->select('status')->where('id',$id_dec)->value('query');
                $appr = DB::table('proker')->select('approved')->where('id',$id_dec)->value('appr');
                @endphp
                
                @if($appr != 1)
                <button type="button" class="btn bg-green-active pull-right disabled"><i class="fa fa-check"></i>
                    Program Terlaksana
                </button>
                @elseif ($query === "Terlaksana")
                <button type="button" class="btn bg-green-active pull-right disabled"><i class="fa fa-check"></i>
                    Program Terlaksana
                </button>
                @else
                <form action="{{url('/proker/detail/' . $id . '/submit')}}" method="post">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success pull-right"><i class="fa fa-check"></i> 
                        Submit Kegiatan
                    </button>
                </form>
                @endif
                
                <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" id="generatePDF">
                    <i class="fa fa-download"></i> Generate PDF
                </button>
            </div>
        </div>
    </section>
    
    
</div>
@endsection

@section('jskonten')
<script>
    $(document).ready(function() {
        $("#generatePDF").click(function() {
            Swal.fire('Sabar ... Fitur masih belum tersedia.')
        });
    });
</script>
@endsection