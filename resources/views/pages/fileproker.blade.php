@extends('layout.app')
@section('konten')
<div class="box box-success">
    <div class="box-header with-border">
        {{-- <h3 class="box-title">Title</h3> --}}
        <div class="box-tools pull-left">
            <h4><i class="fa fa-files-o"></i> Direktori Program Kerja</h4>
        </div>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="col-sm-12">
            @foreach ($file as $f)
            <a href="#">
                <div class="col-sm-4 col-xs-12">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-file-pdf-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{{$f->kegiatan}}</span>
                            
                            <span class="info-box-number">{{date('D, d - M - Y', strtotime($f->tgl_pelaksanaan))}}</span>
                            
                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                <span class="info-box-text">Tahun Pelajaran {{$f->tapel}}</span>
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>
@endsection