@extends('layout.app')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('bootstrap-toggle.min.css')}}">
@endsection
@section('headkonten')
Daftar Pengguna Aplikasi
@endsection
@section('konten')
<div class="box box-success">
    <div class="box-header with-border">
        <a class="btn btn-sm btn-box-tool bg-teal-active" data-toggle="modal" data-target="#AddUser" class="fa fa-plus-circle"></i>&nbsp; Add Pengguna</a>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <table id="TabelUser" class="text-center table table-bordered table-striped nowrap" width="100%">
            <thead>
                <th>#</th>
                <th>E-mail</th>
                <th>Nama Lengkap</th>
                <th>Jabatan</th>
                <th>Status</th>
                <th>Aksi</th>
            </thead>
            <tbody>
                @foreach ($user as $a)
                @php
                $r = $a->role;
                @endphp
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$a->email}}</td>
                    <td>{{$a->name}}</td>
                    <td>
                        @if ($r === "admin")
                        {{"Admin"}}
                        @elseif($r === "kepsek")
                        {{"Kepala Sekolah"}}
                        @elseif($r === "humas")
                        {{"Waka Bidang Humas"}}
                        @elseif($r === "sarpras")
                        {{"Wakabid Sarana & Prasarana"}}
                        @elseif($r === "kesiswaan")
                        {{"Wakabid Kesiswaan"}}
                        @elseif($r === "kurikulum")
                        {{"Wakabid Kurikulum"}}
                        @else
                        {{"-"}}
                        @endif
                        
                    </td>
                    <td>
                        {{-- <span class="label label-success">Aktif</span> --}}
                        <input data-id="{{$a->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $a->status ? 'checked' : '' }}>
                    </td>
                    <td>
                        <a href="#" class="btn btn-sm btn-warning"> Reset Password</a>
                        {{-- <a href="#" class="btn btn-sm btn-danger"> Hapus</a> --}}
                        <a onclick="event.preventDefault();hapususer({{$a->id}});" id="btnProkerFile" class="btn btn-sm btn-danger"> Hapus User</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

@include('partials.adduser')
@endsection

@section('jskonten')
<script src="{{ asset('bootstrap-toggle.min.js')}}"></script>
<script>
    $(function() {
        $('.toggle-class').change(function() {
            var status = $(this).prop('checked') == true ? 1 : 0; 
            var user_id = $(this).data('id'); 
            
            $.ajax({
                type: "GET",
                dataType: "json",
                url: '/pengguna/changestatus',
                data: {'status': status, 'user_id': user_id},
                success: function(data){
                    // console.log(data.success)
                    Swal.fire(
                    'Berhasil!',
                    'Status pengguna berhasil dirubah!',
                    'success'
                    )
                }
            });
        })
    })
    $(document).ready(function(){
        $("#pwd2").keyup(function(){
            if ($("#pwd1").val() != $("#pwd2").val()){
                $("#msg").html("Password harus sama").css("color","red");
                $("#btnsave").prop('disabled', true);
            }else{
                $("#msg").html("Password sesuai").css("color","green").show(1000);
                $("#btnsave").prop('disabled', false);
                
            }
        });
    });
    
    
    function hapususer(id) {
        $.ajax({
            type: 'GET',
            url: '#',
            success: function(data) {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        window.location.reload();
                        Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )
                    }
                })
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>
@endsection
