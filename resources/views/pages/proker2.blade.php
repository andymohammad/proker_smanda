@extends('layout.app')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('alt/DataTables-1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('alt/Responsive-2.2.2/css/responsive.bootstrap.min.css')}}">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('alt/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
<link href='{{ asset('fullcalendar/packages/core/main.css')}}' rel='stylesheet' />
<link href='{{ asset('fullcalendar/packages/daygrid/main.css')}}' rel='stylesheet' />

<script src='{{ asset('fullcalendar/packages/core/main.js')}}'></script>
<script src='{{ asset('fullcalendar/packages/daygrid/main.js')}}'></script>
@endsection
@section('headkonten')
Program Kerja Wakil Kepala Sekolah

@foreach ($role as $a)
<small>Bidang {{$a->role}}</small>
@endforeach

@endsection
@section('konten')
<div class="box box-success">
    <div class="box-header with-border">
        {{-- <h3 class="box-title">Title</h3> --}}
        <div class="box-tools pull-left">
            <h4>Pengajuan Program Kerja</h4>
        </div>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a class="btn btn-sm btn-box-tool bg-teal-active" data-toggle="modal" data-target="#addProker">
            <i class="fa fa-plus-circle"></i> &nbsp; Add Program
        </a>
        <a class="btn btn-sm btn-box-tool bg-blue-active" data-toggle="modal" data-target="#addProker" disabled>
            <i class="fa fa-copy"></i> &nbsp; Copy Program Tahunan
        </a>
        <a class="btn btn-sm btn-box-tool bg-green-active" data-toggle="modal" data-target="#addProker" disabled>
            <i class="fa fa-database"></i> &nbsp; Backup Program Kerja
        </a>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <div class="callout callout-info"> 
            <p>
                <i class="fa fa-info-circle"></i>&nbsp;
                Berikut hanya daftar program kerja anda sesuai tahun pelajaran saat ini ({{$tapel}}), yang <span class="label label-danger">belum diapprove</span> oleh 
                Kepala Sekolah.
            </p>
        </div>
        
        <table id="TabelProker" class="text-center table table-striped table-bordered dt-responsive wrap tabel" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kegiatan</th>
                    <th>Tahun Ajaran</th>
                    <th>Tgl Pelaksanaan</th>
                    <th>Sasaran</th>
                    <th>Status</th>
                    <th>File</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($proker as $p)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$p->kegiatan}}</td>
                    <td>
                        {{$p->tapel}}
                    </td>
                    <td>
                        @if (empty($p->tgl_pelaksanaan))
                        <span class="label label-danger">Belum ditentukan</span>
                        @else
                        {{date('d - m - Y', strtotime($p->tgl_pelaksanaan))}}
                        @endif
                    </td>
                    <td>{{$p->sasaran}}</td>
                    <td>
                        @if ($p->approved === 0)
                        <span class="label label-danger">{{"Ditolak"}}</span>
                        @elseif($p->approved === 1)
                        <span class="label label-success"><i class="fa fa-check-circle"></i> {{"Approved"}}</span>
                        @elseif($p->approved === null)
                        <span class="label label-info"><i class="fa fa-hourglass-start"></i> {{"Waiting"}}</span>
                        @endif
                        <br>
                        @if ($p->status == "Terlaksana")
                        <span class="label label-success"><i class="fa fa-check-circle"></i>{{$p->status}}</span>
                        @else
                        <span class="label label-warning">{{$p->status}}</span>
                        @endif
                    </td>
                    <td>
                        @if (empty($p->file) || $p->file == "-")
                        {{-- <a href="{{route('viewupprok', $p->id)}}" class="label label-danger">Belum upload</a> --}}
                        <a onclick="event.preventDefault();uploadProkerFile({{$p->id}});" id="btnProkerFile" class="label label-danger" data-toggle="modal">Belum upload</a>
                        @else
                        <span class="label label-success"><i class="fa fa-check-circle"></i> Terupload</span>
                        {{-- <a href="{{ route('viewpdf', $p->file)}}" class="label label-success">{{$p->file}}</a> --}}
                        @endif
                    </td>
                    
                    <td>
                        <a href="" onclick="event.preventDefault();editProkerForm({{$p->id}});" class="edit btn btn-xs bg-orange-active" data-toggle="modal" title="Edit Program">
                            <i class="fa fa-edit"></i>
                        </a>
                        @php                            
                        $id = [$p->id,];
                        $id = Crypt::encrypt($id); 
                        @endphp
                        {{-- @if ($p->approved != 1)
                            <a href="" class="btn btn-xs bg-navy color-palette" id="detailproker" disabled title="Detail Program">
                                <i class="fa fa-list"></i>
                            </a>
                            @else
                            <a href="{{ route('detailproker', $id)}}" class="btn btn-xs bg-navy color-palette" id="detailproker" title="Detail Program">
                                <i class="fa fa-list"></i>
                            </a>
                            @endif --}}
                            <a onclick="event.preventDefault();deleteProkerForm({{$p->id}});" href="" class="btn btn-xs bg-red-active delete" data-toggle="modal" title="Hapus Program">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
        @include('partials.proker_add')
        @include('partials.proker_edit')
        @include('partials.proker_delete')
    </div>
    
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="box-tools pull-left">
                <h4>Realisasi Program Kerja</h4>
            </div>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
                <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <table class="text-center table table-striped table-bordered dt-responsive wrap tabel" style="width:100%" id="realisasi">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Program</th>
                        <th>No. SK</th>
                        <th>Tgl Pelaksanaan</th>
                        <th>Anggaran</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($real as $r)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$r->kegiatan}}</td>
                        <td>
                            @php $sk = $r->no_sk @endphp
                            @if ($sk === null)
                            <span class="label label-warning"><i class="fa fa-times"></i> Belum dibuat</span>
                            @else
                            <span class="label label-success">{{$sk}}</span>
                            
                            @endif
                        </td>
                        
                        <td>@if (empty($r->tgl_pelaksanaan))
                            <span class="label label-danger">Belum ditentukan</span>
                            @else
                            {{date('d - m - Y', strtotime($r->tgl_pelaksanaan))}}
                            @endif
                        </td>
                        <td>{{$r->anggaran}}</td>
                        <td>{{$r->status}}</td>
                        <td>
                            <a href="{{ route('detailproker', $id)}}" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Edit Program</a>
                            &nbsp;
                            {{-- <a href="{{ route('detailproker', $id)}}" class="btn btn-xs bg-navy color-palette" id="detailproker" title="Detail Program">
                                <i class="fa fa-list"></i>
                            </a> --}}
                            <a href="" class="btn btn-xs btn-success"><i class="fa fa-check-circle"></i> Submit Program</a>
                            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endsection
    @section('jskonten')
    
    <script src="{{ asset('alt/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('alt/DataTables-1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('alt/Responsive-2.2.2/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('alt/Responsive-2.2.2/js/responsive.bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('proker2.js')}}"></script>
    
    <script src="{{ asset('alt/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
    <script>
        $(document).ready( function () {
            $('input').attr('autocomplete', 'false');
            $('.tabel').DataTable();
            // $('#realisasi').DataTable();
            //Date picker
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'yyyy-mm-dd'
                
            });
            
        } );
    </script>
    @endsection