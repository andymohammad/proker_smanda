@extends('layout.app')
@section('konten')
@foreach ($data as $d)
<div class="row">
    <div class="col-md-3">
        <!-- Profile Image -->
        <div class="box box-primary">
            <div class="box-body box-profile">
                <img class="profile-user-img img-responsive img-circle" src="https://image.flaticon.com/icons/png/512/149/149071.png" alt="User profile picture">
                
                <h3 class="profile-username text-center">{{auth()->user()->name}}</h3>
                
                <p class="text-muted text-center">
                    
                    {{$d->email}}
                    
                </p>
                
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b>Followers</b> <a class="pull-right">1,322</a>
                    </li>
                    <li class="list-group-item">
                        <b>Following</b> <a class="pull-right">543</a>
                    </li>
                    <li class="list-group-item">
                        <b>Friends</b> <a class="pull-right">13,287</a>
                    </li>
                </ul>
                
                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
        
        <!-- About Me Box -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <strong><i class="fa fa-book margin-r-5"></i> Education</strong>
                
                <p class="text-muted">
                    B.S. in Computer Science from the University of Tennessee at Knoxville
                </p>
                
                <hr>
                
                <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
                
                <p class="text-muted">Malibu, California</p>
                
                <hr>
                
                <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
                
                <p>
                    <span class="label label-danger">UI Design</span>
                    <span class="label label-success">Coding</span>
                    <span class="label label-info">Javascript</span>
                    <span class="label label-warning">PHP</span>
                    <span class="label label-primary">Node.js</span>
                </p>
                
                <hr>
                
                <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    
    <div class="col-md-5">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-user"></i>
                <h3 class="box-title">Ubah Data Pengguna</h3>
                <span class="label label-success pull-right">{{$d->id}}</span>
            </div>
            <form action="" method="post" class="form-horizontal">
                <div class="box-body">
                    
                    <div class="form-group">
                        <label class="control-label col-sm-4 col-xs-6">Nama Pengguna</label>
                        <div class="col-sm-8 col-xs-6">
                            <input type="text" class="form-control" value="{{$d->name}}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4 col-xs-6">NIP</label>
                        <div class="col-sm-8 col-xs-6">
                            <input type="text" class="form-control" value="{{$d->nip}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4 col-xs-6">NUPTK</label>
                        <div class="col-sm-8 col-xs-6">
                            <input type="text" class="form-control" value="{{$d->nuptk}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4 col-xs-6">Alamat e-Mail</label>
                        <div class="col-sm-8 col-xs-6">
                            <input type="email" class="form-control" value="{{$d->email}}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-4 col-xs-6">Kontak</label>
                        <div class="col-sm-8 col-xs-6">
                            <input type="text" class="form-control" value="{{$d->kontak}}">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <input type="hidden" name="user_id" value="{{$d->id}}">
                    <button type="submit" class="btn btn-sm bg-blue-active pull-right" disabled>Simpan</button>
                </div>
            </form>
        </div>
    </div>
    
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">
                <i class="fa fa-key"></i>
                <h3 class="box-title">Ubah Password</h3>
            </div>
            <div class="box-body">
                <form action="#" method="post">
                    <div class="form-group">
                        <label class="control-label">Password lama</label>
                        <input type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Password baru</label>
                        <input type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Konfirmasi password baru</label>
                        <input type="password" class="form-control">
                    </div>
                    <div class="box-footer">
                        <a href="#" class="btn btn-sm bg-blue-active pull-right" disabled>
                            Ubah Password
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<table class="table table-striped table-bordered">
    <tbody>
        
        <tr>
            <td>{{$d->email}}</td>
            <td>{{$d->name}}</td>
            <td>{{$d->role}}</td>
        </tr>
        
    </tbody>
</table>
@endforeach
@endsection