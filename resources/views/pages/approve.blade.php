@extends('layout.app')
@section('style')
<link rel="stylesheet" href="{{ asset('alt/DataTables-1.10.18/css/dataTables.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('alt/Responsive-2.2.2/css/responsive.bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('bootstrap-toggle.min.css')}}">
@endsection
@section('headkonten')
Approval Program Kerja
@endsection
@section('konten')
<div class="box box-success">
    <div class="box-header with-border">
        {{-- <h3 class="box-title">Title</h3> --}}
        <button class="btn btn-sm btn-box-tool bg-green" id="btnapproveall" data-toggle="modal" data-target="#ModalApprove">
            <i class="fa fa-check-circle"></i> &nbsp; Approve Semua Pengajuan
        </button>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body table-responsive no-padding">
        <table id="ProkerTable" class="text-center table table-hover" style="width:100%">
            <thead>
                <th>#</th>
                <th>Program Kerja</th>
                <th>Tgl Pelaksanaan</th>
                <th>Koordinator</th>
                <th>Tujuan Program</th>
                <th>Status</th>
                <th>Aksi</th>
            </thead>
            <tbody>
                @foreach ($proker as $a)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$a->kegiatan}}</td>
                    <td>{{$a->tgl_pelaksanaan}}</td>
                    <td>{{ucwords($a->user->role)}}</td>
                    <td>{{$a->tujuan}}</td>
                    <td>
                        @php $g = $a->approved; @endphp
                        @if ($g === 1)
                        <button class="btn btn-sm btn-success btn-circle"><i class="fa fa-check-circle"></i></button>
                        @elseif($g === null)
                        <button class="btn btn-sm btn-warning"><i class="fa fa-hourglass-start"></i> Waiting</button>
                        @elseif($g === 0)
                        <button class="btn btn-sm btn-danger"><i class="fa fa-times-circle"></i> Ditolak</button>
                        @endif
                    </td>
                    <td>
                        <input data-id="{{$a->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Approve" data-off="DisApprove" {{ $a->approved ? 'checked' : '' }}>
                        {{-- @if ($g != 9)
                            <a href="" class="btn btn-sm btn-success" disabled><i class="fa fa-check-circle"></i> Setujui</a>    
                            <a href="" class="btn btn-sm btn-danger" disabled><i class="fa fa-times-circle"></i> Tolak</a>    
                            @else    
                            
                            {{-- <a href="" class="btn btn-sm btn-success"><i class="fa fa-check-circle"></i> Setujui</a>    
                            <a href="" class="btn btn-sm btn-danger"><i class="fa fa-times-circle"></i> Tolak</a>     
                            @endif --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @include('partials.kepsek_approve_all')
    @endsection
    
    @section('jskonten')
    <script src="{{ asset('alt/DataTables-1.10.18/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{ asset('alt/DataTables-1.10.18/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{ asset('alt/Responsive-2.2.2/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{ asset('alt/Responsive-2.2.2/js/responsive.bootstrap.min.js')}}"></script>
    <script src="{{ asset('bootstrap-toggle.min.js')}}"></script>
    
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        })
        $(document).ready( function () {
            //$('#ProkerTable').DataTable();        
        } );
        
        $(function() {
            $('.toggle-class').change(function() {
                var approved = $(this).prop('checked') == true ? 1 : 0; 
                var proker_id = $(this).data('id'); 
                
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '/approve/change',
                    data: {'approved': approved, 'proker_id': proker_id},
                    success: function(data){
                        // console.log(data.success)
                        Toast.fire({
                            type: 'success',
                            title: 'Data Berhasil Diperbarui',
                            timer: 1000
                        })
                    }
                });
            })
        })
    </script>
    @endsection