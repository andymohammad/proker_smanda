@extends('layout.app')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('alt/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('headkonten')
Program Kerja Wakil Kepala Sekolah

@foreach ($role as $a)
<small>Bidang {{$a->role}}</small>
@endforeach

@endsection
@section('konten')
<div class="box box-success">
    <div class="box-header with-border">
        {{-- <h3 class="box-title">Title</h3> --}}
        
        <a class="btn btn-sm btn-box-tool bg-teal-active" data-toggle="modal" data-target="#addProker">
            <i class="fa fa-plus-circle"></i> &nbsp; Add Program
        </a>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <table id="TabelProker" class="text-center table table-striped table-bordered dt-responsive nowrap" style="width:100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Kegiatan</th>
                    <th>Tgl Pelaksanaan</th>
                    <th>Sasaran</th>
                    <th>Status</th>
                    <th>File</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($proker as $p)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$p->kegiatan}}</td>
                    <td>
                        @if (empty($p->tgl_pelaksanaan))
                        <span class="label label-danger">Belum ditentukan</span>
                        @else
                        {{date('d - m - Y', strtotime($p->tgl_pelaksanaan))}}
                        @endif
                        
                    </td>
                    <td>{{$p->sasaran}}</td>
                    <td>
                        <span class="label label-warning">{{$p->status}}</span>
                    </td>
                    
                    <td>
                        @if (empty($p->file) || $p->file == "-")
                        <span class="label label-danger">Belum upload</span>
                        @else
                        {{$p->file}}
                        @endif
                    </td>
                    <td>
                        <a href="" onclick="event.preventDefault();editProkerForm({{$p->id}});" class="edit btn btn-xs bg-orange-active" data-toggle="modal">
                            <i class="fa fa-edit"></i>
                        </a>
                        @php                            
                        $user_id = [$p->user_id,];
                        $user_id = Crypt::encrypt($user_id); 
                        @endphp
                        <a href="#" class="btn btn-xs bg-navy color-palette">
                            <i class="fa fa-list"></i>
                        </a>
                        <a onclick="event.preventDefault();deleteProkerForm({{$p->id}});" href="" class="btn btn-xs bg-red-active delete" data-toggle="modal">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        Footer
    </div>
    <!-- /.box-footer-->
    @include('partials.proker_add')
    @include('partials.proker_edit')
    @include('partials.proker_delete')
</div>
@endsection
@section('jskonten')

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('proker.js')}}"></script>

<script src="{{ asset('alt/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
    $(document).ready( function () {
        
        $('#TabelProker').DataTable();
        //Date picker
        $('.datepicker').datepicker({
            autoclose: true
        })
        
    } );
</script>
@endsection