@php
$id = auth()->user()->id;
$proker = DB::table('proker')->where('user_id',$id)->count();
$sumprok = DB::table('proker')->count();
@endphp

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="https://image.flaticon.com/icons/png/512/149/149071.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{auth()->user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU UTAMA</li>
            <li>
                <a href="/beranda">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            @if (auth()->user()->role == 'kepsek')
            <li>
                <a href="/approve"><i class="fa fa-book"></i> <span>Approval Program Kerja</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-orange">{{$sumprok}}</small>
                    </span>
                </a>
            </li>
            @endif
            @if (auth()->user()->role != 'kepsek')
            <li>
                <a href="/proker2"><i class="fa fa-book"></i> <span>Program Kerja v.2</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-green">{{$proker}}</small>
                    </span>
                </a>
            </li>
            @endif
            @if (auth()->user()->role === 'admin')
            <li><a href="{{ route('konfigurasi')}}"><i class="fa fa-gear"></i> <span>Konfigurasi</span></a></li>
            <li><a href="/pengguna"><i class="fa fa-users"></i> <span>Daftar User</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-calendar-check-o"></i> <span>Rekapitulasi</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('rekaproker')}}"><i class="fa fa-circle-o"></i> Seluruh Program</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Program Terlaksana</a></li>
                </ul>
            </li>
            @endif
            <li><a href="/file-proker"><i class="fa fa-file"></i> <span>File Program Kerja</span></a></li>
            <li><a href="/profil-saya"><i class="fa fa-user"></i> <span>Profil</span></a></li>
            
            {{-- @if (auth()->user()->role === 'admin')
            <li><a href="/proker"><i class="fa fa-book"></i> <span>Program Kerja</span></a></li>
            @endif --}}
            <li><a href="{{ route('logout')}}"><i class="fa fa-coffee"></i><span>Logout</span></a></li>
            {{-- <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> --}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>