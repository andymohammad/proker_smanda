@extends('layout.app')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('konten')
@php
$now = \Carbon\Carbon::now()->format('Y');
$sd = $now + 1;
$tapel = $now . "/" . $sd;
$count = DB::table('config')->count();
@endphp
@if(Session::has('success'))
<script type="text/javascript">
    const Toast = Swal.mixin
    ({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    })
    Swal.fire(
    'Berhasil',
    'Konfigurasi Berhasil Disimpan',
    'success'
    )
</script>
@endif
@if ($count < 1)
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gear"></i>
            &nbsp; Konfigurasi Aplikasi
        </h3>
        
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <form class="form-horizontal" action="{{route('postkonfigurasi')}}" method="POST">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 col-xs-6 control-label">Namaa Sekolah</label>
                    
                    <div class="col-sm-10">
                        <input type="text" name="nama_sekolah" class="form-control" placeholder="Masukkan Nama Sekolah" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-xs-3 control-label">NPSN</label>
                    
                    <div class="col-sm-4">
                        <input type="text" name="npsn" class="form-control" placeholder="Nomor Pokok Nasional Sekolah" required>
                    </div>
                    
                    <label class="col-sm-2 col-xs-3 control-label">Tahun Pelajaran</label>
                    <div class="col-sm-4">
                        <select class="form-control" name="tapel" required>
                            <option value="" disabled selected>&mdash;&nbsp;Tahun Pelajaran&nbsp;&mdash;</option>
                            @for ($i = $now; $i < $now+10; $i++)
                            @php
                            $j = $i + 1
                            @endphp
                            <option value="{{$i . "/" . $j}}">
                                {{$i . " / " . $j}}
                            </option>
                            @endfor
                        </select>
                    </div>
                </div>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn bg-blue-active pull-right">Simpan</button>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <!-- /.box-body -->
    
</div>
@else
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-gear"></i>
            &nbsp; Konfigurasi Aplikasi
        </h3>
        
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        
        <form id="FormUpdateConfig" class="form-horizontal">
            
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 col-xs-6 control-label">Nama Sekolah</label>
                    
                    <div class="col-sm-10">
                        @foreach ($config as $a)
                        <input id="nama_sekolah" type="text" name="nama_sekolah" class="form-control" placeholder="Masukkan Nama Sekolah" disabled value="{{$a->nama_sekolah}}">
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-xs-3 control-label">NPSN</label>
                    
                    <div class="col-sm-4">
                        <input id="npsn" type="text" name="npsn" class="form-control" placeholder="Nomor Pokok Nasional Sekolah" disabled value="{{$npsn}}">
                    </div>                    
                    <label class="col-sm-2 col-xs-3 control-label">Tahun Pelajaran</label>
                    <div class="col-sm-4">
                        <select id="tapel" class="form-control" name="tapel" disabled>
                            @foreach ($config as $a)
                            <option value="{{$a->tapel}}" selected>{{$a->tapel}}</option>
                            @endforeach
                            @for ($i = $now; $i < $now+10; $i++)
                            @php
                            $j = $i + 1
                            @endphp
                            <option value="{{$i . "/" . $j}}">
                                {{$i . " / " . $j}}
                            </option>
                            @endfor
                        </select>
                    </div>
                </div>
                
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <a href="#" class="btn bg-blue-active pull-right edit" data-toggle="modal" data-target="#ModalEditConfig">
                    Ubah Data
                </a>
            </div>
            <!-- /.box-footer -->
        </form>
    </div>
    <!-- /.box-body -->
    
</div>
@endif

@include('partials.config_edit')
@endsection
@section('jskonten')
<script type="text/javascript" src="{{ asset('conf_app.js')}}"></script>
<script type="text/javascript">
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    })
    
</script>

@endsection