@extends('layout.app')
@section('headkonten')
Beranda
<small>Selamat Datang {{auth()->user()->name}}</small>
@endsection
@section('konten')
@if (auth()->user()->role != "kepsek")
@php
$hitunguser = DB::table('users')->count();
@endphp
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="info-box">
            
            <span class="info-box-icon bg-aqua"><i class="fa fa-code-fork"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Program Kerja ANda</span>
                <span class="info-box-number">90<small>%</small></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
            
            <div class="info-box-content">
                <span class="info-box-text">Pengguna Aplikasi</span>
                <span class="info-box-number">{{$hitunguser}}<small> Pengguna</small></span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
</div>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Title</h3>
        
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        Start creating your amazing application!
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        Footer
    </div>
    <!-- /.box-footer-->
</div>    
@else
Kepsek Harap Sabar
@endif

@endsection