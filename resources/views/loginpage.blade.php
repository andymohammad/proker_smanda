<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>JOKERS | Program Kerja Sekolah</title>
    <link rel="icon" href="{{ asset('logo/logosda.png')}}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('alt/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('alt/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('alt/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('alt/css/AdminLTE.min.css')}}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ asset('alt/css/skins/_all-skins.min.css')}}">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            
            <!-- Google Font -->
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
            <link href="https://fonts.googleapis.com/css?family=Courgette&display=swap" rel="stylesheet">
        </head>
        <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
        <body class="hold-transition skin-blue layout-top-nav">
            <div class="wrapper">
                
                <header class="main-header">
                    <nav class="navbar navbar-static-top">
                        <div class="container">
                            <div class="navbar-header">
                                <a href="/" class="navbar-brand" style="font-family: 'Courgette', cursive;">JOKERS &nbsp; | &nbsp; Manajemen Program Kerja Sekolah</a>
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                        <i class="fa fa-bars"></i>
                                    </button>
                                </div>
                                
                                <!-- Collect the nav links, forms, and other content for toggling -->
                                {{-- <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                                        <li><a href="#">Link</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">One more separated link</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <form class="navbar-form navbar-left" role="search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
                                        </div>
                                    </form>
                                </div> --}}
                                <!-- /.navbar-collapse -->
                                <!-- Navbar Right Menu -->
                                <div class="navbar-custom-menu">
                                    <ul class="nav navbar-nav">
                                        <!-- Messages: style can be found in dropdown.less-->
                                        <li>
                                            <a href="/"><i class="fa fa-arrow-left"></i>&nbsp;
                                                <strong>LANDING PAGE</strong>
                                            </a>
                                        </li>
                                        <!-- /.messages-menu -->
                                        
                                        <!-- Notifications Menu -->
                                        {{-- <li class="dropdown notifications-menu">
                                            <!-- Menu toggle button -->
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                <i class="fa fa-bell-o"></i>
                                                <span class="label label-warning">10</span>
                                            </a>
                                        </li> --}}
                                        <!-- Tasks Menu -->
                                        
                                    </ul>
                                </div>
                                <!-- /.navbar-custom-menu -->
                            </div>
                            <!-- /.container-fluid -->
                        </nav>
                    </header>
                    <!-- Full Width Column -->
                    <div class="content-wrapper" style="background: url({{asset('upacara.JPG')}});">
                        <div class="container">
                            <!-- Content Header (Page header) -->
                            {{-- <section class="content-header">
                                <div class="callout callout-info">
                                    <h4>
                                        Sistem Informasi
                                        <small>Program Kerja Wakil Kepala Sekolah</small>
                                    </h4>
                                </div>
                            </section> --}}
                            
                            <!-- Main content -->
                            <section class="content">
                                <div class="login-box">
                                    @if (Session::has('failed'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> Peringatan!</h4>
                                        Username atau Password anda salah
                                    </div>
                                    @endif
                                    <div class="box box-success">
                                        <div class="login-box-body">
                                            <img class="profile-user-img img-responsive img-circle" src="{{ asset('logo/kemdikbud.png')}}" alt="User profile picture">
                                            <br>
                                            <p class="login-box-msg">Masukkan username dan password anda</p>
                                            
                                            <form action="/postlogin" method="post">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="form-group has-feedback">
                                                        <input name="email" type="email" class="form-control col-12" placeholder="Email" required autocomplete="off">
                                                    </div>
                                                    
                                                    <div class="form-group has-feedback">
                                                        <input name="password" type="password" class="form-control col-12" placeholder="Password" required>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
                                                </div>
                                            </form>                                
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="box box-default">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Daftar Program Kerja Wakil Kepala SMAN 2</h3>
                                    </div>
                                    <div class="box-body">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <td>#</td>
                                                    <td>#</td>
                                                    <td>#</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>#</td>
                                                    <td>#</td>
                                                    <td>#</td>
                                                    <td>#</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div> --}}
                                <!-- /.box -->
                            </section>
                            <!-- /.content -->
                        </div>
                        <!-- /.container -->
                    </div>
                    <!-- /.content-wrapper -->
                    <footer class="main-footer">
                        <div class="container">
                            <div class="pull-right hidden-xs">
                                <b>Version</b> 0.1
                            </div>
                            <strong>Copyright &copy; 2019 <a href="https://www.instagram.com/muhammadd.andy/">Muhammad Andy</a>.</strong> All rights
                            reserved.
                        </div>
                        <!-- /.container -->
                    </footer>
                </div>
                <!-- ./wrapper -->
                
                <!-- jQuery 3 -->
                <script src="{{ asset('alt/jquery/dist/jquery.min.js')}}"></script>
                <!-- Bootstrap 3.3.7 -->
                <script src="{{ asset('alt/bootstrap/dist/js/bootstrap.min.js')}}"></script>
                <!-- SlimScroll -->
                <script src="{{ asset('alt/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
                <!-- FastClick -->
                <script src="{{ asset('alt/fastclick/lib/fastclick.js')}}"></script>
                <!-- AdminLTE App -->
                <script src="{{ asset('alt/js/adminlte.min.js')}}"></script>
                <!-- AdminLTE for demo purposes -->
                <script src="{{ asset('alt/js/demo.js')}}"></script>
            </body>
            </html>
            