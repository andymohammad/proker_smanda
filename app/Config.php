<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'config';
    protected $primarykey = 'npsn';
    protected $fillable = [
        'nama_sekolah','npsn','tapel'
    ];
}
