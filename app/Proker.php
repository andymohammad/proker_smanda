<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proker extends Model
{
    protected $table = 'proker';
    protected $fillable = [
        'user_id','kegiatan','tgl_pelaksanaan','sasaran','file','status','tapel','tujuan','indikator',
        'anggaran','proses_me','batas_waktu','sumber_daya','keterangan'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function detailproker()
    {
        return $this->hasMany('App\DetailProker');
    }
}

