<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailProker extends Model
{
    protected $table = 'detail_proker';
    protected $fillable = [
        'proker_id','nama','nip','jabatan','tugas','indikator','batas_waktu','sumber_daya'
    ];

    public function proker()
    {
        return $this->belongsTo('App\Proker');
    }
}
