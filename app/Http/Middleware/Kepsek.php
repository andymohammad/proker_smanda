<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Kepsek
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private $auth;
    public function handle($request, Closure $next)
    {
        // return $next($request);
        $this->auth = auth()->user() ?
        (auth()->user()->role === 'kepsek')
        : false;

        if($this->auth === true)
        return $next($request);
        return redirect('/login')->with('error','Silahkan login terlebih dahulu.');
    }
}
