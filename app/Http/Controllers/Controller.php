<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Config;
class Controller extends BaseController
{
    public function __construct() {
        $tapel = Config::select('tapel')->value('tapel');
        \View::share('tapel', $tapel);
     }
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
