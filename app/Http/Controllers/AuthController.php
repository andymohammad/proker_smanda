<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use App\Config;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(){
        Auth::logout();
        //Session::flush();
        return view('loginpage');
    }
    public function postlogin(Request $r){
        if(Auth::attempt(['email' => $r->email, 'password' => $r->password])){
            $r->session()->regenerate();
            return redirect('/beranda');
        }
        return redirect('/login')->with('failed','Username atau password anda salah');
        // dd($r->all());
    }
    
    public function beranda(){
        $tapel = Config::select('tapel')->value('tapel');
        return view('admin.beranda', compact('tapel'));
    }
    
    public function logout(){
        Auth::logout();
        Session::flush();
        return redirect('/login');

    }
}
