<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Config, DB;

class AdminController extends Controller
{
    public function konfigurasi(){
        $config = Config::all();
        $count = Config::count();
        $npsn = Config::select('npsn')->value('npsn');
        $namsek = Config::select('nama_sekolah')->value('nama_sekolah');
        $tapel = Config::select('tapel')->value('tapel');
        return view('admin.konfigurasi', compact('config','npsn','namsek'));
    }
    public function postkonfigurasi(Request $r){
        $conf = new config;
        $conf->nama_sekolah = $r->nama_sekolah;
        $conf->npsn = $r->npsn;
        $conf->tapel = $r->tapel;
        $conf->save();
        // return view('admin.konfigurasi');
        return redirect()->action('AdminController@konfigurasi')->withSuccess('success');
    }

    public function editconf($npsn){
        $config = Config::find($npsn);
        return response()->json([
            'error' => false,
            'config' => $config,
        ], 200);
    }

    public function updatekonfigurasi(Request $r, $npsn){
        // $proker = Proker::find($id);
        // $proker->kegiatan        =  $request->input('kegiatan');
        // $proker->sasaran        =  $request->input('sasaran');
        // if (!empty($request->tgl_pelaksanaan)) {
        //     $proker->tgl_pelaksanaan = \Carbon\Carbon::createFromFormat('m/d/Y', $request->tgl_pelaksanaan)->format('Y-m-d');
        // }        
        // $proker->save();
        // return response()->json([
        //     'error' => false,
        //     'proker'  => $proker,
        // ], 200);
        //DB::table("players")->where('id_player', $playerId)->first();
        $npsn = $r->input('npsn');
        $nama_sekolah = $r->input('nama_sekolah');
        $tapel = $r->input('tapel');
        $config = DB::table('config')->where('npsn','=',$npsn)->update([
            'npsn'=>$npsn,
            'nama_sekolah'=>$nama_sekolah,
            'tapel' => $tapel,
        ]);

        // $config = Config::findOrFail($npsn);
        // $config->npsn = $r->input('npsn_up');
        // $config->nama_sekolah = $r->input('nama_sekolah_up');
        // $config->tapel = $r->input('tapel_up');
        // $config->save();
        return response()->json([
            'error' => false,
            'config' => $config,
        ], 200);
    }
}
