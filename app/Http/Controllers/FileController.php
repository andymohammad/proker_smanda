<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proker, Auth;
class FileController extends Controller
{
    public function index(){
        $a = Auth::user()->id;
        $file = Proker::where([['file','<>','-'], ['user_id', $a]])->get();
        return view('pages.fileproker', compact('file'));
    }
}
