<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proker;

class KepsekController extends Controller
{
    public function index(){
        $proker = Proker::all()->sortBy('approved');
        
        return view('pages.approve', compact('proker'));
    }
    
    public function changeapprove(Request $r){
        $proker = Proker::find($r->proker_id);
        $proker->approved = $r->approved;
        $proker->save();
        return response()->json(['success'=>'Status change successfully.']);
    }

    public function approveall(Request $r){
        $proker = Proker::where('approved', '=', null)->update(['approved' => '1']);
        return redirect()->route('approve');
    }
}
