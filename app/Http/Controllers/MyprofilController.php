<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Proker;

class MyprofilController extends Controller
{
    public function index(){
        $data = User::where('id', '=', Auth::user()->id)->get();
        return view('pages.myprofil', compact('data'));
    }
}
