<?php

namespace App\Http\Controllers;
use Carbon;
use Illuminate\Http\Request;
use Auth; use Crypt;
use DB;
use App\User; 
use App\Proker; use App\Config;
use App\DetailProker;
use Validator;

class ProkerController extends Controller
{
    
    public function index(){
        $a = Auth::user()->id;
        $data = User::where('id', '=', $a)->get();
        $role = DB::table('users')->where('id','=',$a)->get();
        $proker = Proker::all();
        $tapel = Config::select('tapel')->value('tapel');
        // $proker['tgl_pelaksanaan'] = \Carbon\Carbon::createFromFormat('Y-m-d', $proker->tgl_pelaksanaan)->format('Y-m-d');
        //$users = DB::table('users')->select('name', 'email as user_email')->get();
        //return view('pages.proker', compact('data'));
        return view('pages.proker', compact('proker', 'data','tapel'))->with('role', $role);
        //return $role;
    }
    public function index2(){
        $a = Auth::user()->id;
        $b = Config::select('tapel')->value('tapel');
        $data = User::where('id', '=', $a)->get();
        $role = DB::table('users')->where('id','=',$a)->get();
        $proker = Proker::where([['user_id', $a],[ 'tapel', $b],])->orderBy('tgl_pelaksanaan','ASC')->get();
        $real = Proker::where([['user_id', $a],[ 'tapel', $b],['approved', 1],])->orderBy('tgl_pelaksanaan','ASC')->get();
        // $proker['tgl_pelaksanaan'] = \Carbon\Carbon::createFromFormat('Y-m-d', $proker->tgl_pelaksanaan)->format('Y-m-d');
        //$users = DB::table('users')->select('name', 'email as user_email')->get();
        //return view('pages.proker', compact('data'));
        return view('pages.proker2', compact('proker', 'data','real'))->with('role', $role);
        //return $role;
    }
    
    public function revstore(Request $r){
        $data = new Proker;
        $data->user_id = $r->user_id;
        $data->kegiatan = $r->kegiatan;
        $data->sasaran = $r->sasaran;
        $data->tapel = $r->tapel;
        
        if (!empty($r->tgl_pelaksanaan)) {
            $data['tgl_pelaksanaan'] = \Carbon\Carbon::createFromFormat('Y-m-d', $r->tgl_pelaksanaan)->format('Y-m-d');
        }
        $file = $r->filekegiatan;
        if (!empty($file)) {
            $data->file = $file->getClientOriginalName(); 
            $file->move(public_path('FileProker/'),$file->getClientOriginalName());
        }
        $data->save();
        return redirect('/proker2');
    }
    
    public function store(Request $r){
        $data = $r->all();
        if (!empty($r->tgl_pelaksanaan)) {
            $data['tgl_pelaksanaan'] = \Carbon\Carbon::createFromFormat('m/d/Y', $r->tgl_pelaksanaan)->format('Y-m-d');
        }
        $proker = Proker::create($data);
        
        return $proker;
        return response()->json([
            'error' => false,
            'proker'  => $proker,
        ], 200);
    }
    
    public function show($id){
        $proker = Proker::find($id);
        if (!empty($r->tgl_pelaksanaan)) {
            // $proker->tgl_pelaksanaan = \Carbon\Carbon::createFromFormat('Y-m-d', $proker->tgl_pelaksanaan)->format('m/d/Y');
            $tgl_pelaksanaan = $proker->tgl_pelaksanaan;
        }
        return response()->json([
            'error' => false,
            'proker'  => $proker,
        ], 200);
    }
    
    public function delete($id){
        $proker = Proker::destroy($id);
        
        return response()->json([
            'error' => false,
            'proker'  => $proker,
        ], 200);
    }
    
    public function update(Request $request, $id){
        $validator = Validator::make($request->input(), array(
            'tgl_pelaksanaan' => 'nullable|date',
            'file' => 'nullable',
        ));
        
        if ($validator->fails()) {
            return response()->json([
                'error'    => true,
                'messages' => $validator->errors(),
            ], 422);
        }
        
        $proker = Proker::find($id);
        $proker->kegiatan        =  $request->input('kegiatan');
        $proker->sasaran        =  $request->input('sasaran');
        $proker->no_sk        =  $request->input('sk');
        $proker->tujuan        =  $request->input('tujuan');
        $proker->indikator        =  $request->input('indi');
        if (!empty($request->tgl_pelaksanaan)) {
            $proker->tgl_pelaksanaan = \Carbon\Carbon::createFromFormat('Y-m-d', $request->tgl_pelaksanaan)->format('Y-m-d');
        }
        // $file = $request->input('filekeg');
        // if (!empty($file)) {
        //     $proker->file = $file->getClientOriginalName(); 
        //     $file->move(public_path('FileProker/'),$file->getClientOriginalName());
        // }
        
        $proker->save();
        
        return response()->json([
            'error' => false,
            'proker'  => $proker,
        ], 200);
    }

    public function viewfileproker($id){
        $p = Proker::find($id);
        return response()->json([
            'error' => false,
            'proker' => $p,
        ], 200);
        
    }

    public function uploadfileproker(Request $r){
        $id = $r->proker_id;
        $proker = Proker::find($id);
        $file = $r->uploadfileproker;
        if (!empty($file)) {
            $proker->file = $file->getClientOriginalName(); 
            $file->move(public_path('FileProker/'),$file->getClientOriginalName());
        }
        
        $proker->save();
        return redirect('/proker2');

    }
    
    public function detailproker(Request $r, $id){
        $data = Crypt::decrypt($id);
        $proker = Proker::where('id','=',$data)->get();
        $prokerf = Proker::where('id','=',$data)->first();
        $detproker = DetailProker::where('proker_id','=',$data)->get();
        $detprokerf = DetailProker::where('proker_id','=',$data)->first();
        return view('pages.detail_proker', compact('proker','detproker','id', 'data','detprokerf'));
    }
    
    public function viewpdf(Request $r, $id){
        $data = Crypt::decrypt($id);
        $file = Proker::where('id', $data)->get();
        $file = DB::table('proker')->select('file')->where('id',$data);
        //$path = public_path('FileProker/' . $file);  
        return response()->file(public_path('FileProker/' . $file));
    }
    
    public function submitproker(Request $r, $id){
        $data = Crypt::decrypt($id);
        
        $proker = Proker::find($data);
        
        $proker = Proker::where('id', $data)->update(['status' => 'Terlaksana']);
        return redirect()->route('prokerindex');
    }
    
    public function rekapitulasi(){
        $proker = Proker::all();
        return view('pages.rekap_proker', compact('proker'));
    }
    
}
