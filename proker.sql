-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 19, 2019 at 03:47 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proker`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_proker`
--

CREATE TABLE `detail_proker` (
  `proker_id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `jabatan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `tugas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `tujuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `indikator` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `batas_waktu` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `sumber_daya` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_proker`
--

INSERT INTO `detail_proker` (`proker_id`, `nama`, `nip`, `jabatan`, `tugas`, `tujuan`, `indikator`, `batas_waktu`, `sumber_daya`, `created_at`, `updated_at`) VALUES
(1, '-', '-', '-', '-', '-', '-', '-', '-', '2019-10-18 17:54:08', '2019-10-18 17:54:08');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_05_105211_tabel_proker', 1),
(5, '2019_10_11_121208_tabel_detail_proker', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `proker`
--

CREATE TABLE `proker` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `kegiatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_pelaksanaan` date DEFAULT NULL,
  `sasaran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Belum Terlaksana',
  `file` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '-',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `proker`
--

INSERT INTO `proker` (`id`, `user_id`, `kegiatan`, `tgl_pelaksanaan`, `sasaran`, `status`, `file`, `created_at`, `updated_at`) VALUES
(1, 1, 'Studi Banding Siswa', NULL, 'SMAN 15 Surabaya', 'Terlaksana', '-', '2019-10-18 17:54:08', '2019-10-18 18:12:24'),
(2, 2, 'Penilaian Tengah Semester Ganjil', NULL, 'Siswa', 'Belum Terlaksana', '-', '2019-10-18 17:54:08', '2019-10-18 17:54:08'),
(3, 1, 'Rilis Aplikasi', '2019-10-19', 'SMAN 2 SIDOARJO', 'Belum Terlaksana', '-', '2019-10-18 18:43:34', '2019-10-18 18:43:34'),
(565, 2, 'Est perferendis nemo earum est.', NULL, 'Dr. Olaf Sipes', 'Terlaksana', '-', '2019-10-18 22:01:21', '2019-10-18 22:01:21'),
(566, 2, 'Totam reiciendis et aut.', NULL, 'Gladys Schinner', 'Terlaksana', '-', '2019-10-18 22:01:21', '2019-10-18 22:01:21'),
(567, 3, 'Aut labore fugiat.', NULL, 'Dr. Stuart Kuhn', 'Belum Terlaksana', '-', '2019-10-18 22:01:37', '2019-10-18 22:01:37'),
(568, 3, 'Nihil ipsa suscipit id.', NULL, 'Erwin Crooks DDS', 'Belum Terlaksana', '-', '2019-10-18 22:01:37', '2019-10-18 22:01:37'),
(569, 1, 'Aut id amet commodi non.', NULL, 'Mr. August Mayer', 'Belum Terlaksana', '-', '2019-10-18 22:01:37', '2019-10-18 22:01:37'),
(570, 3, 'Eos iure ipsam quis.', NULL, 'Oral Rogahn', 'Belum Terlaksana', '-', '2019-10-18 22:01:37', '2019-10-18 22:01:37'),
(571, 1, 'Accusantium aut dolor enim.', NULL, 'Ms. Albina Deckow', 'Belum Terlaksana', '-', '2019-10-18 22:01:37', '2019-10-18 22:01:37'),
(572, 3, 'Harum fugiat voluptas culpa.', NULL, 'Mr. Scotty Blanda', 'Belum Terlaksana', '-', '2019-10-18 22:01:50', '2019-10-18 22:01:50'),
(573, 1, 'Ut rerum veritatis.', NULL, 'Philip Cormier', 'Belum Terlaksana', '-', '2019-10-18 22:01:50', '2019-10-18 22:01:50'),
(574, 3, 'Est eligendi dolores nihil blanditiis.', NULL, 'Mrs. Katelyn Ziemann I', 'Belum Terlaksana', '-', '2019-10-18 22:01:50', '2019-10-18 22:01:50'),
(575, 2, 'Et illum mollitia.', NULL, 'Price Effertz', 'Belum Terlaksana', '-', '2019-10-18 22:01:50', '2019-10-18 22:01:50'),
(576, 1, 'Accusamus ullam eaque.', NULL, 'Mrs. Jayda Wiegand III', 'Belum Terlaksana', '-', '2019-10-18 22:01:50', '2019-10-18 22:01:50'),
(577, 3, 'Odit rerum blanditiis et.', NULL, 'Yolanda Roberts', 'Terlaksana', '-', '2019-10-18 22:02:02', '2019-10-18 22:02:02'),
(578, 1, 'Doloremque totam voluptatem.', NULL, 'Rahsaan Treutel II', 'Terlaksana', '-', '2019-10-18 22:02:02', '2019-10-18 22:02:02'),
(579, 2, 'Esse quaerat beatae.', NULL, 'Prof. Anabelle Howe MD', 'Terlaksana', '-', '2019-10-18 22:02:02', '2019-10-18 22:02:02'),
(580, 2, 'Vel dolorem amet eum.', NULL, 'Zion Russel Sr.', 'Terlaksana', '-', '2019-10-18 22:02:02', '2019-10-18 22:02:02'),
(581, 1, 'Incidunt esse accusantium.', NULL, 'Candice Abbott', 'Terlaksana', '-', '2019-10-18 22:02:02', '2019-10-18 22:02:02'),
(582, 3, 'Pariatur deserunt quo aspernatur.', NULL, 'Mortimer Bahringer', 'Terlaksana', '-', '2019-10-18 22:02:05', '2019-10-18 22:02:05'),
(583, 1, 'Distinctio reiciendis ratione.', NULL, 'Misty Hackett IV', 'Terlaksana', '-', '2019-10-18 22:02:05', '2019-10-18 22:02:05'),
(584, 2, 'Quo repudiandae sunt omnis.', NULL, 'Marshall Dibbert', 'Terlaksana', '-', '2019-10-18 22:02:05', '2019-10-18 22:02:05'),
(585, 2, 'Sed exercitationem aut quo.', NULL, 'Ernie Nader', 'Terlaksana', '-', '2019-10-18 22:02:05', '2019-10-18 22:02:05'),
(586, 3, 'Autem fuga sunt optio.', NULL, 'Mr. Johnpaul Corkery DVM', 'Terlaksana', '-', '2019-10-18 22:02:05', '2019-10-18 22:02:05'),
(587, 1, 'Et earum ut.', NULL, 'Hannah Durgan', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(588, 3, 'Ut exercitationem velit unde.', NULL, 'Kole Wolf', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(589, 1, 'Pariatur aspernatur et autem.', NULL, 'Dorothy Parisian', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(590, 1, 'Dolore porro harum fugit.', NULL, 'Leilani Kemmer I', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(591, 1, 'Ut non voluptatem aut.', NULL, 'Dr. Joyce Bode', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(592, 2, 'Enim in totam.', NULL, 'Brannon Durgan', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(593, 3, 'Rerum recusandae sit magnam.', NULL, 'Prof. Jonathon Schmitt PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(594, 2, 'Vero architecto laboriosam qui repudiandae.', NULL, 'Bethel Rice', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(595, 3, 'In ducimus ut.', NULL, 'Karson Prosacco', 'Belum Terlaksana', '-', '2019-10-18 22:02:17', '2019-10-18 22:02:17'),
(596, 2, 'Consequuntur aut.', NULL, 'Nya Kirlin', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(597, 2, 'Nisi id velit cupiditate.', NULL, 'Jean Mitchell', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(598, 2, 'Sit doloribus et repudiandae.', NULL, 'Mrs. Precious Bernier', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(599, 1, 'Nihil perferendis pariatur qui.', NULL, 'Shany Breitenberg', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(600, 3, 'Ex aut vel.', NULL, 'Torrance Schmeler', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(601, 1, 'Maxime omnis non.', NULL, 'Shanny Skiles', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(602, 1, 'Dolor expedita.', NULL, 'Dariana Jakubowski', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(603, 2, 'Explicabo ut recusandae quo.', NULL, 'Ricky Hessel', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(604, 1, 'Assumenda natus et.', NULL, 'Kirk Watsica', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(605, 3, 'Mollitia quo laudantium.', NULL, 'Rudolph Haag', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(606, 1, 'Aut molestias enim.', NULL, 'Dedrick Schiller', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(607, 2, 'Ipsam itaque architecto est.', NULL, 'Prof. Audie Hahn', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(608, 1, 'Suscipit id molestias ab.', NULL, 'Viviane Kling', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(609, 1, 'Sed inventore totam ratione.', NULL, 'Dr. Alexandrea Murazik MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(610, 2, 'Non dolor iure.', NULL, 'Alyson Pfannerstill IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(611, 2, 'Et debitis eum animi delectus.', NULL, 'Mr. Destin Champlin', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(612, 3, 'Error exercitationem magnam sequi.', NULL, 'Grace Little', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(613, 1, 'Eaque maiores at ut.', NULL, 'Drake Rogahn DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(614, 1, 'Et eos voluptatum.', NULL, 'Marjorie Stiedemann', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(615, 3, 'Deserunt eveniet et.', NULL, 'Maureen Corwin', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(616, 2, 'Maxime ut quo dolores.', NULL, 'Mr. Lazaro Macejkovic IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(617, 2, 'Ea delectus consequatur soluta aut.', NULL, 'Justine Runolfsson', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(618, 2, 'Esse dolore modi aspernatur.', NULL, 'Dr. Adella O\'Reilly', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(619, 3, 'Est cupiditate alias.', NULL, 'Juwan Runolfsdottir', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(620, 1, 'Aspernatur mollitia aut ex corrupti.', NULL, 'Nannie Stanton', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(621, 2, 'Excepturi dolore et totam.', NULL, 'Annamarie Haley', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(622, 3, 'Velit sit autem.', NULL, 'Mrs. Clotilde Funk PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(623, 1, 'Quia non hic.', NULL, 'Frida Blick', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(624, 3, 'Velit nemo soluta id.', NULL, 'Tabitha Crona', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(625, 1, 'Voluptatum magnam culpa.', NULL, 'Mr. Wilber Nader', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(626, 3, 'Et cumque impedit voluptate.', NULL, 'Yoshiko Gorczany', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(627, 1, 'Dolorem molestiae corrupti aspernatur.', NULL, 'Miss Amina Ziemann DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(628, 3, 'Tempore qui sed.', NULL, 'Dr. Garrison Russel IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(629, 3, 'Eos iste quisquam in non.', NULL, 'Carmen Deckow', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(630, 1, 'Qui aut consequatur.', NULL, 'Dr. Ethan Bosco', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(631, 3, 'Aspernatur similique.', NULL, 'Maudie Prosacco', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(632, 3, 'Enim provident et.', NULL, 'Lauriane Jones PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(633, 3, 'Sapiente hic quaerat.', NULL, 'Jada Klocko', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(634, 1, 'Animi distinctio magnam.', NULL, 'Jody Kerluke', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(635, 2, 'Incidunt quasi.', NULL, 'Carley Rice', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(636, 3, 'Veritatis omnis.', NULL, 'Mr. Edgardo Corwin MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(637, 3, 'Temporibus a in non facere.', NULL, 'Dr. Trystan Casper Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(638, 3, 'Quas omnis suscipit facilis.', NULL, 'Raquel Strosin', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(639, 1, 'Rem et reiciendis est.', NULL, 'Stephon Schmidt', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(640, 2, 'Voluptatem aliquam.', NULL, 'Mabelle Stokes', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(641, 2, 'Repellat voluptatem at.', NULL, 'Dr. Moses Mitchell Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(642, 2, 'Aut voluptate quod magnam.', NULL, 'Clair Powlowski MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(643, 1, 'Asperiores ut maiores.', NULL, 'Maida Ruecker', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(644, 2, 'Voluptatem cupiditate molestias.', NULL, 'Dr. Jayde O\'Reilly', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(645, 3, 'Aut id exercitationem rem.', NULL, 'Prof. Eriberto Cartwright', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(646, 2, 'Voluptas animi aut.', NULL, 'Annie Jast', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(647, 1, 'Necessitatibus sunt voluptates.', NULL, 'Dr. Erling Lakin DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(648, 1, 'Sequi optio in qui.', NULL, 'Mr. Jessy McLaughlin', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(649, 2, 'Dolores sit dolor.', NULL, 'Yesenia Lind', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(650, 2, 'Nisi exercitationem quas.', NULL, 'Serena Morissette', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(651, 2, 'Nostrum sunt laborum.', NULL, 'Garrick Kuhlman III', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(652, 3, 'Eum quae quis.', NULL, 'Dr. Meggie Luettgen', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(653, 1, 'Tenetur nam repudiandae.', NULL, 'Henderson Halvorson', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(654, 2, 'Sit accusamus.', NULL, 'Effie Spencer', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(655, 3, 'Voluptate ut.', NULL, 'Mark Terry', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(656, 3, 'Commodi voluptate itaque.', NULL, 'Joanne Koch', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(657, 2, 'Velit consectetur et.', NULL, 'Prof. Kaylah Fritsch', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(658, 1, 'Praesentium blanditiis ut quas.', NULL, 'Serenity Von PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(659, 3, 'Ea id eos ipsam.', NULL, 'Dr. Mason Boyle', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(660, 1, 'Deserunt qui dolor.', NULL, 'Rosie Gleason', 'Belum Terlaksana', '-', '2019-10-18 22:02:18', '2019-10-18 22:02:18'),
(661, 1, 'Doloremque illo officia qui.', NULL, 'Chanelle Daugherty', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(662, 2, 'Occaecati eum ut.', NULL, 'Savanna Schneider', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(663, 2, 'In ipsum excepturi autem.', NULL, 'Everett Frami', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(664, 3, 'Minima atque molestiae maxime.', NULL, 'Pete Hills', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(665, 2, 'Aut qui ut.', NULL, 'Freeda Cole', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(666, 3, 'Veniam esse velit.', NULL, 'Antonia Sauer', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(667, 3, 'Illum magni quasi dolor.', NULL, 'Kevin Hayes', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(668, 3, 'Molestiae quae molestiae nobis.', NULL, 'Vidal Heller', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(669, 2, 'Est nam sint.', NULL, 'Rudolph Gerhold', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(670, 1, 'Et omnis itaque.', NULL, 'Brain Farrell', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(671, 3, 'Nihil sunt provident.', NULL, 'Caterina Mayer I', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(672, 1, 'Repellat in dolore est.', NULL, 'Spencer Christiansen', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(673, 1, 'Soluta atque rerum.', NULL, 'Adan Steuber', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(674, 2, 'Optio non qui quia.', NULL, 'Shanna O\'Conner', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(675, 3, 'Voluptatum placeat sapiente.', NULL, 'Vaughn Gutkowski', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(676, 1, 'Architecto esse expedita.', NULL, 'Dr. Emiliano Bayer DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(677, 1, 'Et est occaecati.', NULL, 'Eryn Schneider', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(678, 3, 'Aut voluptas voluptatem numquam.', NULL, 'Stanford Hessel', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(679, 1, 'Voluptates dolor et officia.', NULL, 'Giovanny Altenwerth', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(680, 2, 'Consequatur laboriosam totam molestiae.', NULL, 'Dr. Brendan Jast I', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(681, 3, 'Aperiam quasi asperiores omnis asperiores.', NULL, 'Haskell Hills', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(682, 1, 'Fugit et fuga.', NULL, 'Reuben Kunde III', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(683, 2, 'Provident tempora omnis.', NULL, 'Alfonso Auer', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(684, 2, 'Cupiditate aspernatur.', NULL, 'Ms. Everette Parker II', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(685, 1, 'Incidunt reprehenderit voluptatem recusandae.', NULL, 'Alex Becker', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(686, 1, 'Quas asperiores.', NULL, 'Jeremy Wisozk', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(687, 3, 'Aspernatur perferendis tempore quia.', NULL, 'Madaline Abernathy', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(688, 1, 'Aut maiores qui.', NULL, 'Mariam Gutkowski', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(689, 1, 'Quos voluptatibus deleniti.', NULL, 'Juvenal Runte', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(690, 3, 'Iure sit modi voluptas.', NULL, 'Finn Cronin', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(691, 3, 'Assumenda iusto vel.', NULL, 'Percival Stracke', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(692, 3, 'Iste porro quasi.', NULL, 'Frida Doyle IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(693, 3, 'Repellat est voluptate saepe.', NULL, 'Meta Heller Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(694, 2, 'Ut impedit animi possimus.', NULL, 'Lilian Towne', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(695, 1, 'Consequatur minima tempore.', NULL, 'Mitchell Botsford', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(696, 3, 'Occaecati qui tenetur tempore.', NULL, 'Domenica Braun', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(697, 3, 'Dolor velit nulla excepturi animi.', NULL, 'Thomas DuBuque', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(698, 3, 'Sit autem ipsum dolor rerum.', NULL, 'Brook Will', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(699, 2, 'Aliquam ut aut.', NULL, 'Mrs. Providenci Rodriguez', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(700, 3, 'Dignissimos laudantium ut sed debitis.', NULL, 'Marlon Breitenberg MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(701, 1, 'Aliquam beatae ratione vero.', NULL, 'Erika Wintheiser', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(702, 1, 'Dolores et aperiam.', NULL, 'Miss Abbigail Koch IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(703, 1, 'Est vero modi molestias.', NULL, 'Mrs. Destany Mohr MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(704, 2, 'Voluptate voluptatem ut.', NULL, 'Muhammad Herzog III', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(705, 1, 'Autem qui ea aliquam.', NULL, 'Brayan Lynch', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(706, 1, 'Quidem voluptas in totam.', NULL, 'Armando Hayes II', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(707, 3, 'Voluptas id laborum totam nemo.', NULL, 'Mariana Littel', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(708, 1, 'Eum dolorem deleniti recusandae.', NULL, 'Pascale Mayert PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(709, 3, 'Tempore soluta consequatur.', NULL, 'Cindy Dare Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(710, 1, 'Quo est quaerat.', NULL, 'Marcel Gerlach', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(711, 2, 'Enim soluta voluptas.', NULL, 'Dr. Sister Roob', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(712, 2, 'Ea ad qui.', NULL, 'Keira Armstrong V', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(713, 2, 'Id doloribus omnis aliquam amet.', NULL, 'Johan Jast', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(714, 2, 'Aliquam ut et sit.', NULL, 'Justice Hermann', 'Belum Terlaksana', '-', '2019-10-18 22:02:19', '2019-10-18 22:02:19'),
(715, 1, 'Distinctio ratione molestiae.', NULL, 'Bryon Koss', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(716, 3, 'Adipisci officiis at voluptate.', NULL, 'Jaeden Connelly DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(717, 3, 'Ab sit perferendis et.', NULL, 'Rosanna Parisian', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(718, 3, 'Laboriosam nisi.', NULL, 'Prof. Kamille Brown', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(719, 2, 'Vel ducimus architecto.', NULL, 'Oswaldo Crist', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(720, 1, 'Et quasi dolorem ad.', NULL, 'Tiara Dare Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(721, 2, 'Autem sit eum dicta.', NULL, 'Winona Kulas', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(722, 2, 'Explicabo voluptas qui exercitationem.', NULL, 'Kasey O\'Kon', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(723, 2, 'Est tempora velit ullam.', NULL, 'Freddie Schmeler', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(724, 1, 'Dolores doloremque quia numquam.', NULL, 'Dennis Wolf', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(725, 3, 'Odio explicabo beatae.', NULL, 'Prof. Adam Rippin II', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(726, 1, 'Id non et expedita.', NULL, 'Mrs. Kira Hoeger', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(727, 2, 'Et ut.', NULL, 'Mrs. Rubye Reichert', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(728, 2, 'Qui repudiandae asperiores.', NULL, 'Nikolas Fadel', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(729, 2, 'Sint voluptate aut dignissimos.', NULL, 'Katharina O\'Connell', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(730, 1, 'Non aut et amet.', NULL, 'Mr. Lyric Goodwin', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(731, 2, 'Non et et facilis consequatur.', NULL, 'Aidan Renner', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(732, 2, 'Inventore deleniti nihil.', NULL, 'Clovis Larson IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(733, 3, 'Reiciendis numquam porro.', NULL, 'Erling Hettinger', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(734, 1, 'Dolores magni corrupti voluptatem.', NULL, 'Kayli Spinka I', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(735, 1, 'Sint voluptatibus dolore unde.', NULL, 'Edison Goldner I', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(736, 1, 'Sint incidunt ducimus eius.', NULL, 'Mrs. Winona Douglas IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(737, 3, 'Autem expedita eligendi.', NULL, 'Aiden DuBuque DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(738, 1, 'Eveniet et consequatur aut.', NULL, 'Miss Neoma Lemke PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(739, 2, 'Perferendis velit sit et.', NULL, 'Marietta Cummerata', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(740, 2, 'Et et quam sit reprehenderit.', NULL, 'Suzanne Kuvalis DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(741, 3, 'Quisquam enim veritatis quasi odio.', NULL, 'Quinton Herzog', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(742, 1, 'Et quibusdam voluptate.', NULL, 'Prof. Claudia Nicolas V', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(743, 2, 'Inventore aut dicta magnam et.', NULL, 'Mr. Jayde Farrell', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(744, 3, 'Quibusdam et odio et.', NULL, 'Lexi Jerde', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(745, 2, 'In rerum autem.', NULL, 'Rod Powlowski', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(746, 1, 'Libero sed ut.', NULL, 'Jasper Bins', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(747, 1, 'Voluptatem facilis explicabo.', NULL, 'Prof. Earline Wilkinson', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(748, 2, 'Accusamus et provident.', NULL, 'Jerrold Labadie', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(749, 1, 'Doloremque cum voluptatem praesentium dolores.', NULL, 'Dr. Esteban Lockman II', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(750, 1, 'Fugit modi sed repellat.', NULL, 'Prof. Sheridan Klein Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(751, 3, 'At et quos.', NULL, 'Trenton Langworth', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(752, 2, 'Omnis et sit.', NULL, 'Ms. Maurine Prosacco PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(753, 1, 'Iure quaerat velit qui.', NULL, 'Prof. Nathen Robel', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(754, 2, 'Nesciunt quasi exercitationem optio optio.', NULL, 'Pete Runolfsdottir', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(755, 1, 'Natus cupiditate.', NULL, 'Danyka Langworth', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(756, 1, 'Doloribus quia ipsum.', NULL, 'Caden Hudson', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(757, 2, 'Non fugiat tempora eum.', NULL, 'Mortimer Labadie', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(758, 2, 'Porro velit.', NULL, 'Anahi Medhurst', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(759, 2, 'Officiis ipsum voluptatum odio.', NULL, 'Ms. Violet Robel', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(760, 3, 'Aut rem voluptatem.', NULL, 'Miss Holly Rau V', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(761, 1, 'Molestias illo suscipit ad.', NULL, 'Scotty Howe', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(762, 1, 'Et aliquam praesentium.', NULL, 'Hallie Corwin III', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(763, 3, 'Quo quia doloribus et.', NULL, 'Delphine Eichmann', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(764, 3, 'Eos velit.', NULL, 'Madisyn Langworth Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(765, 1, 'Quia illo tenetur.', NULL, 'Mr. Consuelo Boyer Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(766, 3, 'Non minima aliquam illum.', NULL, 'Prof. Ephraim D\'Amore', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(767, 2, 'Veniam reiciendis velit suscipit.', NULL, 'Moises Prohaska II', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(768, 3, 'Est maiores quidem.', NULL, 'Leann Rempel', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(769, 3, 'Iste rerum accusantium delectus.', NULL, 'Mr. Hazle Erdman', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(770, 2, 'Autem dignissimos animi fuga.', NULL, 'Wayne Ratke', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(771, 2, 'Possimus corporis tempore.', NULL, 'Heidi Hackett', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(772, 1, 'Sit et et a quia.', NULL, 'Immanuel Dare', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(773, 2, 'Aut nam quia.', NULL, 'Abel Greenholt', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(774, 1, 'Libero asperiores.', NULL, 'Prof. Broderick Rodriguez', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(775, 1, 'Et cupiditate sunt.', NULL, 'Glen Lowe V', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(776, 1, 'Debitis illo placeat.', NULL, 'Prof. Arno Pollich', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(777, 1, 'Et provident eaque.', NULL, 'Marcelo McGlynn IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(778, 2, 'Magnam at et quas.', NULL, 'Prof. Dejon Tremblay MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(779, 2, 'Blanditiis ab minima.', NULL, 'Raymundo Gorczany', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(780, 2, 'In corporis nisi perferendis.', NULL, 'Dr. Jaron Greenfelder DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(781, 2, 'Voluptatibus enim beatae sunt dicta.', NULL, 'Prof. Burley Wunsch', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(782, 2, 'Ad quia nisi debitis.', NULL, 'Verla Padberg', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(783, 3, 'Facere aut qui sit.', NULL, 'Lucile Mayert', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(784, 2, 'Inventore aut aut.', NULL, 'Emely Hand', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(785, 1, 'Iste iusto cupiditate.', NULL, 'Chesley Huels', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(786, 1, 'Eos praesentium pariatur nihil.', NULL, 'Gerry Schimmel', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(787, 2, 'Unde distinctio et autem.', NULL, 'Dr. Van Carter MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:20', '2019-10-18 22:02:20'),
(788, 3, 'Eos autem aut accusantium est.', NULL, 'Elwyn Deckow', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(789, 3, 'Illum et similique.', NULL, 'Prof. Erik Gutkowski Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(790, 3, 'Reiciendis neque minima quia.', NULL, 'Mr. Rodrigo Reichel', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(791, 2, 'Ratione cum aliquid omnis.', NULL, 'Milo Thiel', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(792, 2, 'Consequuntur rem reiciendis.', NULL, 'Antonetta Abernathy', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(793, 1, 'Nulla suscipit aperiam.', NULL, 'Remington Denesik V', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(794, 2, 'Porro sed omnis totam.', NULL, 'Ms. Virgie Hintz', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(795, 2, 'Molestiae voluptatem velit.', NULL, 'Emelia Hauck', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(796, 3, 'Consequatur quaerat et exercitationem.', NULL, 'Damian Fay', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(797, 2, 'Atque consequuntur recusandae adipisci.', NULL, 'Elise McDermott', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(798, 2, 'Et velit.', NULL, 'Max Beatty DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(799, 1, 'Quibusdam eum fuga molestias quas.', NULL, 'Jennyfer Price V', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(800, 3, 'Aut autem possimus maiores.', NULL, 'Esther Conn', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(801, 1, 'Sapiente dolores veritatis aut.', NULL, 'Mr. Toy Stracke', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(802, 1, 'Atque quo omnis ut.', NULL, 'Freda Schuster I', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(803, 2, 'Qui hic in.', NULL, 'Waldo Farrell', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(804, 1, 'Voluptatem hic voluptas.', NULL, 'Horacio Stroman', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(805, 2, 'Occaecati officiis ut laboriosam.', NULL, 'David Champlin', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(806, 2, 'Quia cumque magnam.', NULL, 'Claire Roberts', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(807, 3, 'Accusamus eius beatae et.', NULL, 'Dr. Bernhard Tromp Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(808, 1, 'Et nesciunt officiis iure.', NULL, 'Sunny D\'Amore', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(809, 3, 'Nam consequatur quia reiciendis.', NULL, 'Amina Collier DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(810, 3, 'Quo doloribus tenetur est.', NULL, 'Delaney Dach', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(811, 3, 'Quidem provident animi.', NULL, 'Dr. Mina Herzog DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(812, 2, 'Consequatur tempora sint.', NULL, 'Mr. Garth Gibson', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(813, 3, 'Adipisci et quo.', NULL, 'Creola Schneider Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(814, 2, 'Qui nisi repellat temporibus.', NULL, 'Miss Erna Kling I', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(815, 2, 'Tempore inventore quae.', NULL, 'Garett Gutmann', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(816, 2, 'Quae est nihil.', NULL, 'Dr. Cale Hartmann', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(817, 1, 'Sapiente ullam aspernatur libero.', NULL, 'Dr. Retta Effertz DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(818, 2, 'Dolores non.', NULL, 'Joelle Steuber', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(819, 2, 'Culpa aut sit.', NULL, 'Simone Brown', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(820, 1, 'Temporibus atque quis.', NULL, 'Prof. Garland Jakubowski', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(821, 1, 'Odit blanditiis non nesciunt explicabo.', NULL, 'Darwin Dietrich', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(822, 1, 'Perspiciatis dolorem rem est.', NULL, 'Mrs. Vicenta Murazik III', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(823, 3, 'Delectus aut sed.', NULL, 'Samantha Lehner MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(824, 1, 'Et modi eum.', NULL, 'Caden Reilly Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(825, 2, 'Sunt aliquam aut vero.', NULL, 'Brandon Murphy', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(826, 1, 'Natus odit nostrum.', NULL, 'Dr. Otha Bailey', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(827, 2, 'Inventore libero.', NULL, 'Mr. Barney Bechtelar', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(828, 1, 'Ipsa reiciendis accusantium.', NULL, 'Lemuel Spinka', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(829, 1, 'Aliquid odit ea.', NULL, 'Narciso Farrell', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(830, 2, 'Nesciunt voluptatem harum.', NULL, 'Dr. Ulises Hodkiewicz', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(831, 1, 'Corrupti totam deleniti minima.', NULL, 'Virgil Douglas', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(832, 2, 'Et ut autem.', NULL, 'Sabryna Cruickshank Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(833, 3, 'Id ipsa ipsum est.', NULL, 'Kathleen Smitham II', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(834, 1, 'Ipsum qui.', NULL, 'Eli Kreiger', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(835, 2, 'Sequi id ad.', NULL, 'Miss Danyka Schumm DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(836, 1, 'Quam sunt.', NULL, 'Prof. Charity Konopelski', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(837, 2, 'Asperiores autem at.', NULL, 'Una Watsica', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(838, 2, 'Qui similique perferendis adipisci.', NULL, 'Prof. Rafael Schamberger', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(839, 2, 'Aut est.', NULL, 'Naomi Metz PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(840, 2, 'Ab non.', NULL, 'Nora Bergnaum', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(841, 2, 'Consequatur praesentium quibusdam neque.', NULL, 'Dr. Stephan O\'Hara V', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(842, 1, 'Aperiam consequatur consequatur.', NULL, 'Murray Franecki', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(843, 2, 'Qui veniam velit.', NULL, 'Dominic Greenholt III', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(844, 1, 'Cumque aut aut quo earum.', NULL, 'Dewayne Batz Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(845, 2, 'Ipsum dolor reprehenderit officia.', NULL, 'Ms. Grace Konopelski DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(846, 2, 'Aperiam sed nisi.', NULL, 'Fiona Stracke Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(847, 2, 'Iusto non et tempora.', NULL, 'Makenzie Zemlak DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(848, 1, 'Ad consequuntur qui consectetur.', NULL, 'Jennifer Collier', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(849, 2, 'Ex ex dicta vitae.', NULL, 'Doug Donnelly Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(850, 3, 'Earum eum iste inventore explicabo.', NULL, 'Prof. Jefferey Lebsack DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(851, 1, 'Optio corrupti.', NULL, 'Prof. Trever Brown', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(852, 1, 'Tenetur tempore doloremque itaque.', NULL, 'Kari Kub', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(853, 2, 'Quisquam ipsum consectetur qui.', NULL, 'Prof. Kirk Schaden DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(854, 3, 'Sit vel sapiente deleniti.', NULL, 'Loyce Smith Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(855, 3, 'Praesentium rerum ipsa.', NULL, 'Alta Hintz', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(856, 3, 'Possimus sint voluptas.', NULL, 'Mavis Hudson', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(857, 3, 'Omnis et.', NULL, 'Imelda Anderson', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(858, 1, 'Vel veniam earum asperiores.', NULL, 'Winston Steuber', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(859, 1, 'Magnam error quia.', NULL, 'Glenna McClure', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(860, 2, 'Hic quae ea.', NULL, 'Cale Larkin', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(861, 1, 'Esse unde molestiae quaerat.', NULL, 'Abe Rosenbaum Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(862, 1, 'Est ut temporibus.', NULL, 'Tiffany Crist MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(863, 1, 'Atque porro quidem ut.', NULL, 'Camren Ryan', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(864, 2, 'Dolore eaque et quo.', NULL, 'Everett Botsford', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(865, 3, 'At debitis laboriosam qui.', NULL, 'Dr. Jayde Hills', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(866, 3, 'Quis quo sit voluptatibus.', NULL, 'Ms. Arvilla Lind V', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(867, 2, 'Eum cupiditate.', NULL, 'Nikki Bruen', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(868, 1, 'Qui dolorem eligendi maxime.', NULL, 'Dr. Bernita Donnelly III', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(869, 1, 'Sed voluptate vel.', NULL, 'Rey Kihn', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(870, 3, 'Consequatur aliquam est quod.', NULL, 'Dr. Mackenzie Jones Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:21', '2019-10-18 22:02:21'),
(871, 3, 'Id est.', NULL, 'Ms. Delphia Parker DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(872, 3, 'Tempore iusto corrupti.', NULL, 'Zakary Rempel', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(873, 2, 'Aut consequuntur.', NULL, 'Dr. Arnoldo Hoppe', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(874, 2, 'Molestiae ut perferendis.', NULL, 'Brenden Bahringer', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(875, 1, 'Sequi qui expedita nisi.', NULL, 'Delphine Treutel I', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(876, 1, 'Nisi sed vel.', NULL, 'Timmy Herzog', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(877, 1, 'Quam nihil non doloribus.', NULL, 'Jeff Rogahn', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(878, 3, 'Maiores in deserunt explicabo.', NULL, 'Cassandre Feeney', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(879, 1, 'Sit ex velit.', NULL, 'Dr. Nolan Wunsch', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(880, 1, 'Omnis ut quam.', NULL, 'Noemi Bins', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(881, 3, 'Animi veniam sit reprehenderit.', NULL, 'Doyle Cole', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(882, 1, 'Ut alias assumenda molestiae.', NULL, 'Loyce Mohr', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(883, 2, 'Ipsa qui sunt laborum vero.', NULL, 'Mr. Magnus Ankunding', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(884, 3, 'Alias ducimus distinctio facere.', NULL, 'Ivah Harber', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(885, 2, 'Dicta qui perferendis.', NULL, 'Mr. Nathen Tillman', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(886, 1, 'Omnis culpa maxime earum.', NULL, 'Prof. Earnestine Kerluke', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(887, 3, 'Doloribus iure omnis illo.', NULL, 'Althea Bernier MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(888, 1, 'Eaque non in.', NULL, 'Dr. Darrion Welch', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(889, 2, 'Veritatis temporibus voluptatem aut.', NULL, 'Dr. Sister Kuvalis II', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(890, 2, 'Non rem quod omnis.', NULL, 'Clovis Rath', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(891, 1, 'In qui error ullam.', NULL, 'Yasmine Connelly I', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(892, 2, 'Est corporis perspiciatis omnis pariatur.', NULL, 'Ignacio Stoltenberg', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(893, 3, 'Et tempore quia dolor.', NULL, 'Lavina Lowe V', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(894, 3, 'Illo sint reiciendis atque.', NULL, 'Jacques Bednar', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(895, 3, 'Unde occaecati.', NULL, 'Dr. Scarlett Roberts MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(896, 3, 'Laudantium sit porro odio.', NULL, 'Dr. Kariane Altenwerth', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(897, 2, 'Saepe modi.', NULL, 'Emilie Quigley', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(898, 2, 'Voluptas dolorem atque.', NULL, 'Dr. Norval Bins', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(899, 2, 'Iure numquam omnis aut.', NULL, 'Sigrid Kuvalis', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(900, 3, 'Sequi provident possimus perferendis rerum.', NULL, 'Arlie Thiel', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(901, 2, 'Minus pariatur cum.', NULL, 'Abbie Marks', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(902, 1, 'Magnam id optio.', NULL, 'Lucius Konopelski', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(903, 1, 'Est eum facilis.', NULL, 'Dr. Leanne Harvey II', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(904, 3, 'Libero omnis veritatis eum.', NULL, 'Miss Audrey Braun V', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(905, 2, 'Eum nemo rerum animi animi.', NULL, 'Mr. Furman Carter', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(906, 2, 'Pariatur dolores nulla officia hic.', NULL, 'Maybelle Crooks MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(907, 3, 'Doloribus fugit consequuntur.', NULL, 'Elliot O\'Conner I', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(908, 1, 'Sunt eligendi.', NULL, 'Arlo Graham', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(909, 2, 'Aut voluptatibus aut.', NULL, 'Ms. Herminia Grady Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(910, 1, 'Sed consequatur eius et.', NULL, 'Hollie Nader', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(911, 3, 'Placeat natus voluptate.', NULL, 'Prof. Marianna Wyman I', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(912, 1, 'Ea velit inventore quis.', NULL, 'Antonette Kirlin', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(913, 3, 'Qui ut ipsum.', NULL, 'Hubert Mueller MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(914, 1, 'Fugit voluptas soluta consequatur.', NULL, 'Lydia Hackett', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(915, 3, 'Suscipit pariatur nesciunt veritatis et.', NULL, 'Landen Medhurst MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(916, 3, 'Ex sequi eos.', NULL, 'Hillary Collins', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(917, 2, 'Sint odit omnis.', NULL, 'Jillian Ankunding', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(918, 2, 'Voluptas dolores accusamus.', NULL, 'Lorenzo White', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(919, 1, 'Et sapiente id illum.', NULL, 'Prof. Alysha Veum DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(920, 3, 'Debitis sint et esse.', NULL, 'Maryjane Jerde', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(921, 3, 'Atque veritatis.', NULL, 'Bradley Schuppe', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(922, 2, 'Est reiciendis suscipit.', NULL, 'Santino Treutel', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(923, 2, 'Quam doloremque ex ut.', NULL, 'Garret Wintheiser', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(924, 1, 'Repudiandae ut velit odio.', NULL, 'Mrs. Kallie McClure', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(925, 3, 'Assumenda est veritatis ut quidem.', NULL, 'Olin Prohaska', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(926, 3, 'Repellendus doloribus error.', NULL, 'Mr. Noe Quigley', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(927, 3, 'Eius quod alias.', NULL, 'Prof. Kendra Hermiston I', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(928, 3, 'Ipsa fugiat consectetur.', NULL, 'Thea Blanda', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(929, 3, 'Porro blanditiis cupiditate iure.', NULL, 'Dr. Edison Watsica Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(930, 1, 'Doloremque assumenda quod.', NULL, 'Dr. Susie Doyle', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(931, 3, 'Quia magni sunt excepturi.', NULL, 'Evans Schultz', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(932, 2, 'Harum recusandae commodi placeat excepturi.', NULL, 'Emelia Harris', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(933, 2, 'Nesciunt ducimus.', NULL, 'Mustafa Crooks', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(934, 1, 'Ea laudantium commodi.', NULL, 'Ayana Cartwright', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(935, 3, 'Animi est.', NULL, 'Abagail Willms', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(936, 3, 'Suscipit vero quam.', NULL, 'Heaven Hagenes', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(937, 2, 'Mollitia consequuntur est.', NULL, 'Miss Marisol Jerde', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22');
INSERT INTO `proker` (`id`, `user_id`, `kegiatan`, `tgl_pelaksanaan`, `sasaran`, `status`, `file`, `created_at`, `updated_at`) VALUES
(938, 1, 'Cum et omnis nulla.', NULL, 'Claire Stehr', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(939, 2, 'Et aut quisquam.', NULL, 'Lenny Streich', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(940, 2, 'Earum ipsam quia nam.', NULL, 'Dr. Deon Cummerata V', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(941, 1, 'Quaerat sunt hic aut.', NULL, 'Miss Jayda Abernathy', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(942, 2, 'Dolorem error eum aut.', NULL, 'Onie Mueller', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(943, 2, 'Iure neque ea saepe.', NULL, 'Prof. Eriberto Runolfsdottir', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(944, 1, 'Dolore fugiat aut.', NULL, 'Torey Daniel', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(945, 3, 'Unde aut laboriosam et.', NULL, 'Adelbert Tremblay PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(946, 1, 'Sed ipsum tempora.', NULL, 'Dejuan Lueilwitz I', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(947, 2, 'Placeat eum aut.', NULL, 'Damaris Hartmann', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(948, 2, 'Et dolore nostrum cum.', NULL, 'Dr. Clifford Conroy Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:22', '2019-10-18 22:02:22'),
(949, 2, 'Voluptatem quibusdam incidunt.', NULL, 'Reginald Cormier', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(950, 1, 'Sed dolor aut voluptatibus.', NULL, 'Wiley Ziemann', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(951, 1, 'Rerum vero esse quia.', NULL, 'Tristin Maggio', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(952, 3, 'Sunt doloribus.', NULL, 'Magnus Buckridge', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(953, 1, 'Velit expedita eligendi.', NULL, 'Larry Schuster', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(954, 3, 'Aspernatur qui qui est.', NULL, 'Ephraim Bartoletti', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(955, 2, 'Qui et veritatis.', NULL, 'Alene Rogahn', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(956, 2, 'Ut similique quod sit.', NULL, 'Mrs. Ciara Pfeffer', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(957, 3, 'Vel pariatur quis harum ipsam.', NULL, 'Ardith Lynch', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(958, 2, 'Ullam iste voluptatem corporis.', NULL, 'Jeremie Lehner II', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(959, 3, 'Laudantium est blanditiis.', NULL, 'Edd Swift III', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(960, 1, 'Consequatur consequatur nisi omnis.', NULL, 'Abraham O\'Reilly PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(961, 3, 'Quas et sed.', NULL, 'Prof. Cornell Kihn', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(962, 3, 'Voluptas sit in dicta.', NULL, 'Fernando Bruen', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(963, 1, 'Ut laudantium aspernatur cupiditate.', NULL, 'Demetris Kulas', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(964, 2, 'Incidunt animi tempora rerum.', NULL, 'Hildegard Beahan', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(965, 3, 'Suscipit culpa dicta.', NULL, 'Clare Hoppe', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(966, 3, 'Voluptatum dolores nostrum eius ipsa.', NULL, 'Prof. Marco Bode Jr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(967, 3, 'Veniam voluptatibus aut.', NULL, 'Keon Mills MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(968, 2, 'Sequi facere aut non vel.', NULL, 'Tyrel Runte', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(969, 2, 'Ut maxime qui.', NULL, 'Domenico Grady', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(970, 3, 'Et consequatur rerum culpa.', NULL, 'David Runolfsson', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(971, 1, 'Nostrum voluptas ut nostrum.', NULL, 'Clinton Orn', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(972, 2, 'Rem odit ipsa autem.', NULL, 'Tad Murazik', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(973, 2, 'Eos sunt reiciendis sint repudiandae.', NULL, 'Mr. Angel Kling Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(974, 3, 'Voluptatum eaque quae.', NULL, 'Hailee Wiza', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(975, 2, 'Fuga a.', NULL, 'Edison Oberbrunner', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(976, 2, 'Est a debitis et.', NULL, 'Christophe Cummings', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(977, 3, 'Deleniti voluptatem cupiditate ut reiciendis.', NULL, 'Prof. Linnie Corkery', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(978, 2, 'Suscipit non mollitia voluptatem.', NULL, 'Elise Gleichner V', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(979, 2, 'Recusandae tempora provident consectetur.', NULL, 'Prof. Genevieve Hyatt V', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(980, 3, 'Illo ut.', NULL, 'Jarod Hodkiewicz Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(981, 1, 'Ipsum aut cupiditate.', NULL, 'Isidro Green', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(982, 2, 'Omnis minus maxime.', NULL, 'Mr. Luigi Williamson DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(983, 3, 'Eum totam dicta.', NULL, 'Hilma Kuvalis DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(984, 3, 'Quo sed incidunt officiis explicabo.', NULL, 'Lula Wilkinson', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(985, 1, 'Est vitae asperiores ea.', NULL, 'Patience Harvey', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(986, 3, 'Eos enim excepturi maiores.', NULL, 'Prof. Eryn Ullrich', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(987, 3, 'Voluptate nisi consequatur.', NULL, 'Jaeden Purdy', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(988, 3, 'Et in et quo.', NULL, 'Mr. Kennith Robel', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(989, 2, 'Qui ut aliquam non et.', NULL, 'Priscilla Koelpin', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(990, 3, 'Est ipsa.', NULL, 'Manuela Ryan', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(991, 3, 'Tenetur beatae.', NULL, 'Morton Harber', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(992, 3, 'Qui sit velit.', NULL, 'Carmella Koch', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(993, 2, 'In blanditiis fuga reprehenderit.', NULL, 'Rodolfo O\'Keefe', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(994, 1, 'Maxime eveniet accusantium consequatur.', NULL, 'Adriel Hoppe', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(995, 2, 'Facere soluta nisi neque.', NULL, 'Pat Crona', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(996, 1, 'Minima suscipit quo laborum.', NULL, 'Rachel Johns', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(997, 3, 'Est voluptas accusamus.', NULL, 'Marlee Wolf', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(998, 3, 'Voluptate et vel et.', NULL, 'Lue Rodriguez III', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(999, 3, 'Aut officia aspernatur error.', NULL, 'Leone Kertzmann', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1000, 2, 'Qui et tenetur.', NULL, 'Moises King', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1001, 2, 'Quis consequatur laudantium et eos.', NULL, 'Micaela Corkery', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1002, 1, 'Non dolorum reiciendis.', NULL, 'Anika Erdman Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1003, 1, 'Quidem molestias voluptatem dolores.', NULL, 'Nicole Kohler', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1004, 2, 'Voluptatem deserunt nihil.', NULL, 'Judy Effertz', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1005, 3, 'Rem quo vero magni.', NULL, 'Dr. Gregory Harris PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1006, 2, 'Necessitatibus consequatur aliquid vero.', NULL, 'Jaquan Medhurst', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1007, 1, 'Iusto totam eum sed.', NULL, 'Mohammad Boyer', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1008, 3, 'Sint aspernatur debitis.', NULL, 'Carmen Swift', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1009, 2, 'Reprehenderit amet ratione.', NULL, 'Jazlyn Schoen', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1010, 3, 'Architecto veniam aut.', NULL, 'Cecelia Bednar III', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1011, 3, 'Est voluptas molestias.', NULL, 'Dr. Guy Lang Sr.', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1012, 2, 'Ut similique in sit rem.', NULL, 'Mr. Elmore Beahan PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1013, 3, 'Est ut laudantium.', NULL, 'Hans Borer', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1014, 2, 'Autem quibusdam non ut.', NULL, 'Mr. Peyton Kuvalis', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1015, 3, 'Placeat iusto nobis sunt ducimus.', NULL, 'Dr. Omari Hartmann PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1016, 2, 'Magni consequuntur ducimus.', NULL, 'Tessie Dach', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1017, 3, 'Temporibus consequatur eligendi.', NULL, 'Hertha Lang', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1018, 2, 'Nesciunt temporibus.', NULL, 'Grady Erdman', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1019, 2, 'Adipisci voluptate aliquid.', NULL, 'Guadalupe Wuckert', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1020, 2, 'Maiores adipisci nesciunt.', NULL, 'Kirsten Grimes', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1021, 2, 'Tenetur qui est aut.', NULL, 'Arjun Emmerich', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1022, 2, 'Eos consequatur rerum nam tempore.', NULL, 'Prof. Ora Jast IV', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1023, 2, 'Voluptatem dolor.', NULL, 'Isabell Pfannerstill', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1024, 1, 'Dolorum itaque asperiores.', NULL, 'Renee Barton', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1025, 3, 'Occaecati qui.', NULL, 'Abagail Collins', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1026, 1, 'Adipisci et laudantium.', NULL, 'Juston Durgan', 'Belum Terlaksana', '-', '2019-10-18 22:02:23', '2019-10-18 22:02:23'),
(1027, 1, 'Nisi atque fuga commodi.', NULL, 'Dorian Carroll', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1028, 3, 'Alias labore autem.', NULL, 'Ottilie Runolfsson DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1029, 2, 'Sed voluptas corrupti.', NULL, 'Howard Kreiger', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1030, 3, 'Natus aspernatur suscipit.', NULL, 'Prof. Nicolette Wunsch', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1031, 3, 'Rerum consequatur recusandae deleniti.', NULL, 'Madeline Stokes PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1032, 2, 'Ut quasi corporis.', NULL, 'Talia Lehner', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1033, 1, 'Ipsum qui aperiam veritatis.', NULL, 'Meagan Block', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1034, 2, 'Omnis unde aperiam doloribus exercitationem.', NULL, 'Sunny Hoeger', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1035, 3, 'Est qui repudiandae ratione.', NULL, 'Dr. Cristina Dickinson DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1036, 3, 'Iste sunt nihil perferendis.', NULL, 'Icie McKenzie', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1037, 1, 'Laboriosam consequuntur consequatur.', NULL, 'Archibald Murazik', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1038, 2, 'Id neque non in.', NULL, 'Alverta Weimann', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1039, 1, 'Ratione dolore voluptates.', NULL, 'Carlos Cormier I', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1040, 2, 'Odio voluptas consequatur distinctio ullam.', NULL, 'Prof. Myrtie Witting', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1041, 3, 'Aliquid culpa facilis molestiae.', NULL, 'Prof. Hershel Braun II', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1042, 2, 'Iusto a libero repudiandae.', NULL, 'Leora Greenfelder', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1043, 2, 'Aut aut omnis eum.', NULL, 'Horace Barrows', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1044, 1, 'Aut praesentium tempore nobis.', NULL, 'Jaden Bailey', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1045, 1, 'Sunt ea rerum cum.', NULL, 'Webster Kohler MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1046, 1, 'Molestias est.', NULL, 'Jamarcus Quigley DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1047, 1, 'Animi consequatur rem ratione.', NULL, 'Rico Zieme', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1048, 2, 'Voluptatem quis perspiciatis sit.', NULL, 'Michaela Brakus', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1049, 2, 'Consequatur labore et.', NULL, 'Lowell Ullrich', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1050, 1, 'Voluptas amet vel.', NULL, 'Kallie Schroeder', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1051, 2, 'Sed id vel quasi voluptatem.', NULL, 'Allene Nolan', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1052, 3, 'Illum error voluptatem ut.', NULL, 'Mae Schuster', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1053, 1, 'Quia laboriosam sapiente.', NULL, 'Lillie Breitenberg', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1054, 3, 'Enim occaecati velit.', NULL, 'Harmon Runolfsdottir', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1055, 2, 'Delectus pariatur perferendis.', NULL, 'Miss Breanne Hoeger III', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1056, 2, 'Fugiat velit asperiores enim.', NULL, 'Dr. Hester Anderson I', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1057, 2, 'Repellendus quam eius est sunt.', NULL, 'Margarette Gibson', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1058, 3, 'Tempore beatae est occaecati.', NULL, 'Prof. Malika Lowe', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1059, 3, 'Qui eum et.', NULL, 'Everardo Corwin', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1060, 1, 'Repudiandae saepe corporis.', NULL, 'Miss Connie Huels', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1061, 3, 'Magnam possimus voluptates est.', NULL, 'Prof. Isac Rempel DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1062, 2, 'Excepturi ut ea quo.', NULL, 'Alicia Klein MD', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1063, 3, 'Quos sit est.', NULL, 'Gerson Sawayn', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1064, 3, 'Aut vel voluptatem.', NULL, 'Mrs. Jodie Douglas V', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1065, 3, 'Neque accusamus vitae inventore.', NULL, 'Ursula Hills', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1066, 3, 'Alias neque.', NULL, 'Stan Crona', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1067, 1, 'Qui nemo est.', NULL, 'Lavern Hermann', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1068, 3, 'In ad blanditiis cumque.', NULL, 'Prof. Dion Lowe DVM', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1069, 3, 'Iusto qui iure ut sunt.', NULL, 'Ms. Verda Cruickshank', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1070, 2, 'Nemo iusto laboriosam soluta.', NULL, 'Prof. Micheal Gusikowski PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1071, 1, 'Et ipsa quis.', NULL, 'Shania Auer', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1072, 2, 'Est est autem in.', NULL, 'Vergie Lemke II', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1073, 3, 'Qui ratione laboriosam.', NULL, 'Prof. Lucious Cartwright', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1074, 2, 'Ipsum exercitationem.', NULL, 'Gwen Lebsack', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1075, 1, 'Expedita quas quia.', NULL, 'Dr. Dena Hill DDS', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1076, 1, 'Earum dolorem aut.', NULL, 'Peggie Jenkins', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1077, 1, 'Nihil dolor et veritatis.', NULL, 'Sim Conn', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1078, 2, 'Expedita consequatur ipsam delectus.', NULL, 'Stanton Gislason', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1079, 1, 'Rem ex tempore consequatur accusamus.', NULL, 'Mrs. Amya Murphy', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1080, 1, 'Voluptatem aspernatur perferendis libero odio.', NULL, 'Dr. Floyd Torphy PhD', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1081, 2, 'Veniam quasi sed possimus.', NULL, 'Luisa Lowe', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1082, 3, 'Dolor corrupti nihil.', NULL, 'Sidney O\'Kon', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1083, 3, 'Rerum voluptate et quaerat et.', NULL, 'Addie Beahan', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1084, 3, 'Nihil voluptatum assumenda qui.', NULL, 'Gene Corkery', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1085, 2, 'Est ut perferendis.', NULL, 'Sylvan Lueilwitz', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24'),
(1086, 1, 'Quae eius.', NULL, 'Prof. Marley Heathcote III', 'Belum Terlaksana', '-', '2019-10-18 22:02:24', '2019-10-18 22:02:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` enum('admin','kurikulum','humas','sarpras','kepsek','komite','user') COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Andy Mohammad Teguh', 'andy@andy', NULL, '$2y$10$5CBnaXPTe4I0vP5SFgPlRuxSVYQcb2e8J.PI.sF4vBvQMFZnVk49C', NULL, '2019-10-18 17:54:06', '2019-10-18 17:54:06'),
(2, 'kurikulum', 'Basman', 'basman@basman', NULL, '$2y$10$RenHpVMBo.cNO6plMGeN0u1hS0Dezyk9vu.GcNHWRbBfZuFKbILsS', NULL, '2019-10-18 17:54:06', '2019-10-18 17:54:06'),
(3, 'humas', 'Sokran', 'sokran@sokran', NULL, '$2y$10$KR9/FkThLvYbvo63LwYNJOk6so3NEv0fLt9sKSJQ1QXOPD8K2qKwu', NULL, '2019-10-18 17:54:06', '2019-10-18 17:54:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_proker`
--
ALTER TABLE `detail_proker`
  ADD KEY `detail_proker_proker_id_foreign` (`proker_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `proker`
--
ALTER TABLE `proker`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proker_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `proker`
--
ALTER TABLE `proker`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1087;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=529;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_proker`
--
ALTER TABLE `detail_proker`
  ADD CONSTRAINT `detail_proker_proker_id_foreign` FOREIGN KEY (`proker_id`) REFERENCES `proker` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `proker`
--
ALTER TABLE `proker`
  ADD CONSTRAINT `proker_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
