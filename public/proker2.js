const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})
$(document).ready(function() {
    $("#btn-edit").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var formData = new FormData(document.getElementById("FormEditProker"));
        $.ajax({
            type: 'PUT',
            url: '/proker/' + $("#FormEditProker input[name=proker_id]").val(),
            data: {
                sasaran: $("#FormEditProker input[name=sasaran]").val(),
                kegiatan: $("#FormEditProker input[name=kegiatan]").val(),
                tgl_pelaksanaan: $("#FormEditProker input[name=tgl_pelaksanaan]").val(),
                sk: $("#FormEditProker input[name=sk]").val(),
                tujuan: $("#FormEditProker input[name=tujuan]").val(),
                indi: $("#FormEditProker input[name=indi]").val(),
                //file: $("#FormEditProker input[name=filekeg]").val(),
            },
            formData,
            dataType: 'json',            
            success: function(data) {
                $('#FormEditProker').trigger("reset");
                $("#FormEditProker .closemodal").click();
                window.location.reload();
                Toast.fire({
                    type: 'success',
                    title: 'Data Berhasil Diperbarui',
                    timer: 6000
                })
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#edit-proker-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#edit-proker-errors').append('<li>' + value + '</li>');
                });
                $("#edit-error-bag").show();
            }
        });
    });
    $("#btn-delete").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/proker/' + $("#FormDeleteProker input[name=proker_id]").val(),
            dataType: 'json',
            success: function(data) {
                $("#FormDeleteProker .closemodal").click();
                window.location.reload();
                Toast.fire({
                    type: 'success',
                    title: 'Data Berhasil Dihapus'
                })
            },
            error: function(data) {
                console.log(data);
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Cek kembali data yang anda masukkan!',
                })
            }
        });
    });
});

function addTaskForm() {
    $(document).ready(function() {
        $("#add-error-bag").hide();
        $('#addTaskModal').modal('show');
    });
}

function editProkerForm(task_id) {
    $.ajax({
        type: 'GET',
        url: '/proker/' + task_id,
        success: function(data) {
            $("#edit-error-bag").hide();
            $("#FormEditProker input[name=tgl_pelaksanaan]").val(data.proker.tgl_pelaksanaan);
            $("#FormEditProker input[name=kegiatan]").val(data.proker.kegiatan);
            $("#FormEditProker input[name=sasaran]").val(data.proker.sasaran);
            $("#FormEditProker input[name=proker_id]").val(data.proker.id);
            $('#ModalEditProker').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}

function deleteProkerForm(proker_id) {
    $.ajax({
        type: 'GET',
        url: '/proker/' + proker_id,
        success: function(data) {
            $("#FormDeleteProker #delete-title").html("Hapus Kegiatan ?");
            $("#FormDeleteProker input[name=proker_id]").val(data.proker.id);
            $("#FormDeleteProker input[name=kegiatan]").val(data.proker.kegiatan);
            $('#ModalDeleteProker').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}




function uploadProkerFile(proker_id) {
    $.ajax({
        type: 'GET',
        url: '/proker/file/' + proker_id,
        success: function(data) {
            $("#FormFileProker #file-title").html("Upload file program kerja");
            $("#FormFileProker input[name=proker_id]").val(data.proker.id);
            // $("#FormFileProker input[name=uploadfileproker]").val('asas');
            // $("#FormFileProker input[name=kegiatan]").val(data.proker.kegiatan);
            $('#ModalFileProker').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}