$(document).ready(function(){
    $('#btnsaveconf').click(function(){
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
        $.ajax({
            type: 'PUT',
            url: '/konfigurasi/' + $("#FormEditConfig input[name=npsn_up]").val(),
            data:{
                npsn: $("#FormEditConfig input[name=npsn_up]").val(),
                nama_sekolah: $("#FormEditConfig input[name=nama_sekolah_up]").val(),
                tapel: $("#FormEditConfig select[name=tapel_up]").val(),
            },
            dataType: 'json',
            success: function(data){
                $('#FormEditConfig').trigger("reset");
                $("#FormEditConfig .closemodal").click();
                window.location.reload();
            },
            error: function(data){
                var errors = $.parseJSON(data.responseText);
                $('#edit-proker-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#edit-proker-errors').append('<li>' + value + '</li>');
                });
                $("#edit-error-bag").show();
            }
        });
    });
});

function editConForm(npsn){
    $.ajax({
        type: 'GET',
        url: '/konfigurasi/' + npsn,
        success: function(data){
            $("#edit-error-bag").hide();
            $("#FormEditConfig input[name=npsn]").val(data.config.npsn);
            $("#FormEditConfig input[name=nama_sekolah]").val(data.config.nama_sekolah);
            $("#FormEditConfig input[name=tapel]").val(data.config.npsn);
            $('#ModalEditConfig').modal('show');
        },
        error: function(data){
            console.log(data);
        }
    });
}