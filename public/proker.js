$(document).ready(function() {
    
    $("#btn-edit").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'PUT',
            url: '/proker/' + $("#FormEditProker input[name=proker_id]").val(),
            data: {
                tgl_pelaksanaan: $("#FormEditProker input[name=tgl_pelaksanaan]").val(),
                kegiatan: $("#FormEditProker input[name=kegiatan]").val(),
                sasaran: $("#FormEditProker input[name=sasaran]").val(),
            },
            dataType: 'json',
            success: function(data) {
                $('#FormEditProker').trigger("reset");
                $("#FormEditProker .close").click();
                window.location.reload();
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#edit-proker-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#edit-proker-errors').append('<li>' + value + '</li>');
                });
                $("#edit-error-bag").show();
            }
        });
    });
    $("#btn-delete").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'DELETE',
            url: '/proker/' + $("#FormDeleteProker input[name=proker_id]").val(),
            dataType: 'json',
            success: function(data) {
                $("#FormDeleteProker .close").click();
                window.location.reload();
            },
            error: function(data) {
                console.log(data);
            }
        });
    });
    $("#btn-add").click(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '/proker/add',
            data: {
                user_id: $("#FormAddProker input[name=user_id]").val(),
                tgl_pelaksanaan: $("#FormAddProker input[name=tgl_pelaksanaan]").val(),
                kegiatan: $("#FormAddProker input[name=kegiatan]").val(),
                sasaran: $("#FormAddProker input[name=sasaran]").val(),
                filekegiatan: $("#FormAddProker input[name=filekegiatan]").val(),                
            },
            dataType: 'json',
            success: function(data) {
                $('#FormAddProker').trigger("reset");
                $("#FormAddProker .close").click();
                window.location.reload();
            },
            error: function(data) {
                var errors = $.parseJSON(data.responseText);
                $('#add-task-errors').html('');
                $.each(errors.messages, function(key, value) {
                    $('#add-task-errors').append('<li>' + value + '</li>');
                });
                $("#add-error-bag").show();
            }
        });
    });
});

function addTaskForm() {
    $(document).ready(function() {
        $("#add-error-bag").hide();
        $('#addTaskModal').modal('show');
    });
}

function editProkerForm(task_id) {
    $.ajax({
        type: 'GET',
        url: '/proker/' + task_id,
        success: function(data) {
            $("#edit-error-bag").hide();
            $("#FormEditProker input[name=tgl_pelaksanaan]").val(data.proker.tgl_pelaksanaan);
            $("#FormEditProker input[name=kegiatan]").val(data.proker.kegiatan);
            $("#FormEditProker input[name=sasaran]").val(data.proker.sasaran);
            $("#FormEditProker input[name=proker_id]").val(data.proker.id);
            $('#ModalEditProker').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}

function deleteProkerForm(proker_id) {
    $.ajax({
        type: 'GET',
        url: '/proker/' + proker_id,
        success: function(data) {
            $("#FormDeleteProker #delete-title").html("Hapus Kegiatan ?");
            $("#FormDeleteProker input[name=proker_id]").val(data.proker.id);
            $("#FormDeleteProker input[name=kegiatan]").val(data.proker.kegiatan);
            $('#ModalDeleteProker').modal('show');
        },
        error: function(data) {
            console.log(data);
        }
    });
}