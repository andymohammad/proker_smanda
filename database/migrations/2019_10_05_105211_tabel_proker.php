<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Proker;

class TabelProker extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('proker', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('kegiatan');
            $table->date('tgl_pelaksanaan')->nullable();
            $table->string('sasaran');
            $table->string('status', 20)->default('Belum Terlaksana');
            $table->tinyInteger('approved')->nullable();
            $table->string('no_sk', 100)->nullable();
            $table->string('tujuan')->nullable();
            $table->string('indikator')->nullable();
            $table->string('anggaran', 100)->nullable();
            $table->string('file', 100)->nullable()->default('-');
            $table->string('proses_me')->nullable();
            $table->string('batas_waktu')->nullable();
            $table->string('sumber_daya')->nullable();
            $table->string('tapel', 12);
            $table->string('keterangan')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamps();
        });
        
        Proker::create([
            'user_id' => '1',
            'kegiatan' => 'Studi Banding Siswa',
            'sasaran' => 'SMAN 15 Surabaya',
            'tapel' => '2019/2020',
            ]);
            Proker::create([
                'user_id' => '2',
                'kegiatan' => 'Penilaian Tengah Semester Ganjil',
                'sasaran' => 'Siswa',
                'tapel' => '2019/2020',
                ]);
            }
            
            /**
            * Reverse the migrations.
            *
            * @return void
            */
            public function down()
            {
                Schema::dropIfExists('proker');
            }
        }
        