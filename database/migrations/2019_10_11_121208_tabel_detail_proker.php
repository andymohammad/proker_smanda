<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\DetailProker;

class TabelDetailProker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_proker', function (Blueprint $table) {
            $table->UnsignedbigInteger('proker_id');
            $table->string('nama')->nullable()->default('-');
            $table->string('nip')->nullable()->default('-');
            $table->string('jabatan')->nullable()->default('-');
            $table->string('tugas')->nullable()->default('-');
            $table->string('tujuan')->nullable()->default('-');
            $table->string('indikator')->nullable()->default('-');
            $table->string('batas_waktu')->nullable()->default('-');
            $table->string('sumber_daya')->nullable()->default('-');
            $table->timestamps();

            $table->foreign('proker_id')->references('id')->on('proker')
            ->onDelete('cascade')->onUpdate('cascade');
        });
        DetailProker::create([
            'proker_id' => '1',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_proker');
    }
}
