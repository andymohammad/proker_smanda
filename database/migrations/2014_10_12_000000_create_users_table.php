<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;

class CreateUsersTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('role', ["admin","kurikulum","kesiswaan","humas","sarpras","kepsek","komite","user"]);
            $table->string('name');
            $table->string('nip')->unique();
            $table->string('nuptk')->unique();
            $table->string('kontak', 20);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('status')->nullable()->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
        User::create([
            'role' => 'admin',
            'name' => 'Andy Mohammad Teguh',
            'nip' => rand(111111111111111111, 999999999999999999),
            'nuptk' => rand(111111111111111111, 999999999999999999),
            'kontak' => rand(111111111111111111, 999999999999999999),
            'email' => 'andy@andy',
            'password' => bcrypt('rahasia'),
            ]);
            
            User::create([
                'role' => 'kurikulum',
                'name' => 'Basman',
                'nip' => rand(111111111111111111, 999999999999999999),
                'nuptk' => rand(111111111111111111, 999999999999999999),
                'kontak' => rand(111111111111111111, 999999999999999999),
                'email' => 'basman@basman',
                'password' => bcrypt('rahasia'),
                ]);
                User::create([
                    'role' => 'kepsek',
                    'name' => 'Kepala Sekolah',
                    'nip' => rand(111111111111111111, 999999999999999999),
                    'nuptk' => rand(111111111111111111, 999999999999999999),
                    'kontak' => rand(111111111111111111, 999999999999999999),
                    'email' => 'kepsek@kepsek',
                    'password' => bcrypt('rahasia'),
                    ]);
                    User::create([
                        'role' => 'humas',
                        'name' => 'Hubungan Masyarakat',
                        'nip' => rand(111111111111111111, 999999999999999999),
                        'nuptk' => rand(111111111111111111, 999999999999999999),
                        'kontak' => rand(111111111111111111, 999999999999999999),
                        'email' => 'humas@humas',
                        'password' => bcrypt('rahasia'),
                        ]);
                        
                        User::create([
                            'role' => 'humas',
                            'name' => 'Sokran',
                            'nip' => rand(111111111111111111, 999999999999999999),
                            'nuptk' => rand(111111111111111111, 999999999999999999),
                            'kontak' => rand(111111111111111111, 999999999999999999),
                            'email' => 'sokran@sokran',
                            'password' => bcrypt('rahasia'),
                            ]);
                            User::create([
                                'role' => 'admin',
                                'name' => 'Kartolo',
                                'nip' => rand(111111111111111111, 999999999999999999),
                                'nuptk' => rand(111111111111111111, 999999999999999999),
                                'kontak' => rand(111111111111111111, 999999999999999999),
                                'email' => 'admin@admin',
                                'password' => bcrypt('rahasia'),
                                ]);
                            }
                            
                            /**
                            * Reverse the migrations.
                            *
                            * @return void
                            */
                            public function down()
                            {
                                Schema::dropIfExists('users');
                            }
                        }
                        