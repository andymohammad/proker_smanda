<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Proker;
use Faker\Generator as Faker;

$factory->define(Proker::class, function (Faker $faker) {
    return [
        'user_id' => rand(1,6),
        'tgl_pelaksanaan' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+5 months'),
        'tujuan' => $faker->sentence(5),
        'kegiatan' => $faker->sentence(3),
        'sasaran' => $faker->name(),
        'tapel' => '2019/2020',
        'status' => 'Belum Terlaksana',
    ];
});